# Texi2HTML Rectified #

This repository contains a modification of 
[texi2html.py/texi2hh](https://github.com/python/cpython/blob/3.11/Tools/scripts/texi2html.py), 
which can be used to generate CHM and EPUB files in the style of 
[Python25.chm](https://www.python.org/ftp/python/2.5.4/Python25.chm) and its predecessors.&ensp; My
modifications were worked on in 2011–2012, then 2020–2022 (yes, that is an 8-year gap), then 2024;
the script's general messiness is partly my fault, but substantially inherited from the original.

Trying to browse the documentation for (for example) NodeJS in their usual format is a rather
stressful experience, I've found every time I've tried.&ensp; While losing one's place by clicking
links can be resolved with the back button, losing one's place completely by slightly nudging the
scrollbar cannot trivially be reversed, so navigating a single very long page for a module can be
stressful.&ensp; Python 2.5 and earlier used a one-node-per-page format in their CHM format
documentation (and their HTML-format documentation, although the main disadvantage of this 
approach—inability to search the entire thing—is moot for CHM or EPUB, viewers for which usually
allows searching the entire book); however, this is no longer the case in Python 2.6 or later, due
to changing documentation system from its custom LaTeX2HTML setup to Sphinx, and as such the
documentation for all remotely modern Python versions are in a format more analogous to the NodeJS
documentation.&ensp; This fork of `texi2html.py` is my approach to addressing this, since
`texi2html.py` also uses a one-node-per-page format, and existing toolchains exist for getting the
documentation in Texinfo format.

For generating the input `.texi` from NodeJS docs or documentation with a similar Markdown-based
system, see [here](https://github.com/gromnitsky/md2texi).

The standard Sphinx toolchain can be used for the Python documentation:

```bash
$ sphinx-build -b texinfo Python-3.12.3/Doc python-3.12
$ cp assets/* python-3.12 # Copy the CSS and navigation icons into the build directory
$ P fancy_bits.py -b5E python-3.12 python-3.12/python.texi python-3.12
```

Since the files from which most of this is derived come from various versions of the Python source
tree, the terms of distribution are believed to be the same as [for Python 
itself](https://docs.python.org/3/license.html).&ensp; I impose no additional terms besides those 
on further modification or distribution.&ensp; Note that `regex_weburl.py` is derived from elsewhere
and as such has its own terms.

## Dependencies ##

[`html5lib`](https://pypi.org/project/html5lib/) is used when building EPUB, as part of the process
of converting the generated HTML to XHTML for inclusion in the EPUB.

## Usage ##

```
{texi2html_rectified.py|fancy_bits.py} [-d [-d]] [-p] [-c] [-5] [[-b] -H htmlhelp || -E epub]
                                       inputfile outputdirectory
```

* `-d`: print more verbose debugging information.
* `-p`: verbosely print all headers to stdout as they are encountered.
* `-c`: include link to continuation node (next node in sequence) as well as next node (at
  level).&ensp; Has no effect when invoking via `fancy_bits.py` (see below).
* `-5`: use HTML5 features in output.
* `-b`: when used with `-H`, invoke the CHM file compiler (`chmcmd` or, on Windows, `hhc`) after
  writing the CHM metadata files.
* `-H`: output CHM metadata/project files with the specified basename.&ensp; Cannot be used at the
  same time as `-E`.
* `-E`: output EPUB metadata files and an EPUB file with the specified basename.&ensp; Cannot be
  used at the same time as `-H`.
* `inputfile`: a `.texi` file to read in from.
* `outputdirectory`: the directory to write the HTML to.

Execute `fancy_bits.py` to use the navigation bar chrome from `Python25.chm` (create the
destination directory and copy the contents of the `assets` directory first).&ensp; Otherwise,
invoke `texi2html_rectified.py` directly.&ensp; The invocation is essentially the same, except that
the `-c` option to enable continuation node links is ignored when `fancy_bits.py` is used (since
the `Python25.chm` style bars use different Next/Previous semantics, where Next is roughly 
equivalent to the Texinfo Cont where the latter is applicable—the Texinfo-semantic Next/Previous
are instead retained as "Next Over"/"Previous Over" links in cases where they differ, as an
extension to the basic `Python25.chm` navigation bar layout).

## Changes ##

Erroring out when the `-H` option is not used has been fixed.&ensp; Compatibility with Sphinx's 
output for the Python documentation has been improved.&ensp; Unicode is treated as something that
exists rather than approximating output as ASCII (but escapes are used for non-ASCII in CHM since
handling in some client/OS setups seemed inconsistent otherwise).&ensp; Links to anchors are made
to work properly.&ensp; Generation of tables of contents has been reworked to a certain extent on
the general principle of node headings with section numbering being a more appropriate thing to
display to the reader than the internal node names.

`fancy_bits.py` is added, based on `Python25.chm`.

The CHM-project generating `-H basename` option is retained, with the addition of a `-b` option to
execute the CHM-file compiler.&ensp; However, since CHM is somewhat
of a dated format nowadays, an option (`-E basename`) is also provided to generate EPUB 
instead.&ensp; Note that, although the NCX format allows zero or more flat lists in addition to the
hierarchal table of contents, viewers often ignore this feature; hence, although both contents and
index are included in both the CHM and EPUB outputs, a viewer (e.g. `kchmviewer`) may well display
the index tab for the CHM but not the EPUB.

The HTML 3.0 output mode has been removed.&ensp; HTML 3.0 is one of the ghost versions of HTML, 
like XHTML 2.0 after it: it was rejected as a standard, with some of its features cannibalised by
later versions (but not all, with `<lh>` being a notable feature which still does not exist,
although its semantic function is arguably fulfilled by `<figcaption>`).&ensp;
HTML 2 (as the original defaulted to) was superseded by HTML 3.2, then by HTML 4, then by HTML
5.&ensp; I have changed it to output HTML 4 by default, with a `-5` option to output HTML 5.&ensp; 
This option should not be used when generating CHM.




