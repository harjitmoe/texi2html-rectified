#! /usr/bin/env python3
# -*- mode: python; coding: utf-8 -*-

# Convert GNU texinfo files into HTML, one file per node.
# Based on Texinfo 2.14.
# Usage: texi2html [-d [-d]] [-p] [-c] [-5] [[-b] -H htmlhelp || -E epub]
#                  inputfile outputdirectory
# The input file must be a complete texinfo file, e.g. emacs.texi.
# This creates many files (one per info node) in the output directory,
# overwriting existing files of the same name.  All files created have
# ".html" as their extension.

# Edits and extentions: HarJIT, 2011, 2012, 2020, 2021, 2022, 2024.
#  Mostly, though not entirely, in aid of generating alternative
#  CHMs for Python docs via Texinfo.
# Also added an EPUB target, since CHM is a fairly obsolete
#  format even for Windows targets, and e.g. kchmviewer supports
#  viewing EPUBs with a CHM-style contents/search sidebar.
# Also some updates: HTML 2.0 is purely historic, HTML 3.0 never
#  happened, HTML 4.0 is the main legacy for older viewers, and
#  HTML 5 Living is the current version.
# The remainder of the comment blurb present in the version from
#  the Python distribution (in Tools/scripts) is retained below.

# XXX To do:
# - handle @comment*** correctly
# - handle @xref {some words} correctly
# - handle @ftable correctly (items aren't indexed?)
# - handle @itemx properly
# - handle @exdent properly
# - add links directly to the proper line from indices
# - check against the definitive list of @-cmds; we still miss (among others):
# - @defindex (hard)
# - @c(omment) in the middle of a line (rarely used)
# - @this* (not really needed, only used in headers anyway)
# - @today{} (ever used outside title page?)

# More consistent handling of chapters/sections/etc.
# Lots of documentation
# Many more options:
#       -top    designate top node
#       -links  customize which types of links are included
#       -split  split at chapters or sections instead of nodes
#       -name   Allow different types of filename handling. Non unix systems
#               will have problems with long node names
#       ...
# Support the most recent texinfo version and take a good look at HTML 3.0
# More debugging output (customizable) and more flexible error handling
# How about icons ?

# rpyron 2002-05-07
# Robert Pyron <rpyron@alum.mit.edu>
# 1. BUGFIX: In function makefile(), strip blanks from the nodename.
#    This is necessary to match the behavior of parser.makeref() and
#    parser.do_node().
# 2. BUGFIX fixed KeyError in end_ifset (well, I may have just made
#    it go away, rather than fix it)
# 3. BUGFIX allow @menu and menu items inside @ifset or @ifclear
# 4. Support added for:
#       @uref        URL reference
#       @image       image file reference (see note below)
#       @multitable  output an HTML table
#       @vtable
# 5. Partial support for accents, to match MAKEINFO output
# 6. I added a new command-line option, '-H basename', to specify
#    HTML Help output. This will cause three files to be created
#    in the current directory:
#       `basename`.hhp  HTML Help Workshop project file
#       `basename`.hhc  Contents file for the project
#       `basename`.hhk  Index file for the project
#    When fed into HTML Help Workshop, the resulting file will be
#    named `basename`.chm.
# 7. A new class, HTMLHelp, to accomplish item 6.
# 8. Various calls to HTMLHelp functions.
# A NOTE ON IMAGES: Just as 'outputdirectory' must exist before
# running this program, all referenced images must already exist
# in outputdirectory.

import os, sys, string, re, subprocess, platform, binascii, secrets, uuid, zipfile, io, getopt, urllib.parse, locale
import html5lib
from regex_weburl import re_weburl

MAGIC = '\\input texinfo'

cmprog = re.compile('^@([a-z]+)([ \t]|$)')        # Command (line-oriented)
blprog = re.compile('^[ \t]*$')                   # Blank line
kwprog = re.compile('@[a-z]+')                    # Keyword (embedded, usually
                                                  # with {} args)
spprog = re.compile('[\n@{}&<>]|``|\'\'|---|_{66}')# Special characters in
                                                  # running text
                                                  #
                                                  # menu item (Yuck!)
miprog = re.compile(r'^\* ([^:]*):(:|[ \t]*([^\t,\n.]+)([^ \t\n]*))[ \t\n]*')
#                    0    1     1 2        3          34         42        0
#                          -----            ----------  ---------
#                                  -|-----------------------------
#                     -----------------------------------------------------


class HTMLNode:
    """Some of the parser's functionality is separated into this class.

    A Node accumulates its contents, takes care of links to other Nodes
    and saves itself when it is finished and all links are resolved.
    """

    DOCTYPE = '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">'

    type = 0
    cont = rcont = ''
    epilogue = '</body></html>\n'
    css = ""
    nulls = (None, "", "(dir)")
    gui_assets = ()

    def __init__(self, dir, name, topname, title, next_node, prev, up, charset, friendlies, index_node, has_modindex):
        self.dirname = dir
        self.name = name
        if topname:
            self.topname = topname
        else:
            self.topname = name
        self.title = title
        self.next = next_node
        self.prev = prev
        self.up = up
        self.lines = []
        self.charset = charset
        self.release_info = None # Will be populated by the parser
        self.friendlies = friendlies
        self.index_node = index_node
        self.has_modindex = has_modindex

    def write(self, *lines):
        for line in lines:
            self.lines.append(line)

    def flush(self):
        with open(self.dirname + '/' + makefile(self.name), 'w',
                encoding=self.charset, errors="xmlcharrefreplace") as fp:
            fp.write(self.prologue)
            fp.write(self.text)
            fp.write(self.epilogue)

    def clean_name(self, nodename):
        nodename = self.friendlies.get(nodename, nodename) if self.friendlies else nodename
        return nodename.replace('&','&amp;').replace('<', '&lt;')\
                       .replace('>', '&gt;').replace('"', '&quot;')

    def clean_title(self):
        return self.title.replace(self.name, self.clean_name(self.name))

    def link(self, label=None, nodename=None, rel=None, rev=None, element="a", label_element=None,
             class_=None, label_class=None):
        if nodename == None and label != None:
            nodename = label
            label = None
        if nodename:
            if nodename.lower() == '(dir)':
                addr = '../dir.html'
                title = ''
            else:
                addr = makefile(nodename)
                cleanname = self.clean_name(nodename)
                title = ' title="%s"' % cleanname
            if label != None:
                self.write(' ')
                if label_element:
                    self.write('<', label_element, 
                       label_class and (' class=' + label_class) or "", '>')
                self.write(label, ': ')
                if label_element:
                    self.write('</', label_element, '>')
            self.write('<', element, ' href="', addr, '"', \
                       rel and (' rel=' + rel) or "", \
                       rev and (' rev=' + rev) or "", \
                       class_ and (' class=' + class_) or "", \
                       title, '>')
            if element.lower() != "link":
                self.write(cleanname, '</', element, '>  \n')
            else:
                self.write('\n')

    def finalize(self):
        length = len(self.lines)
        self.text = ''.join(self.lines)
        self.lines = []
        self.open_links(True)
        self.output_links()
        self.close_links()
        links_hdr = ''.join(self.lines)
        self.lines = []
        self.open_links(False)
        self.output_links()
        self.close_links()
        links_ftr = ''.join(self.lines)
        # Link elements (takes effect in handful of browsers e.g. lynx)
        # There are only two types applicable here by standard (back and next)
        self.lines = []
        self.link(self.cont, rel="next", element="link")
        next_link = ''.join(self.lines)
        self.lines = []
        self.link(self.prev if self.prev not in self.nulls else self.up, rel="prev", element="link")
        back_link = ''.join(self.lines)
        # Generating prologue and epilogue
        self.lines = []
        self.prologue = (
            self.DOCTYPE +
            '\n<html><head>\n'
            '  <!-- Converted with texi2html and Python -->\n'
            '  ' + self.output_charset() + '\n'
            '  <meta http-equiv="X-UA-Compatible" content="IE=edge">\n'
            '  <title>' + self.clean_title() + '</title>\n'
            '  ' + next_link + '\n'
            '  ' + back_link + '\n' + self.css +
            '</head><body>\n' +
            links_hdr)
        if length > 20:
            self.epilogue = '\n%s%s</body></html>\n' % (links_ftr, self.get_release_info())
        else:
            self.epilogue = '\n%s</body></html>\n' % self.get_release_info()
    
    def get_release_info(self):
        return "<div>" + self.release_info + "</div>"

    def output_links(self):
        self.link(' | Prev', self.prev, rel='prev')
        self.link(' | Up', self.up)
        if self.name != self.topname:
            self.link(' | Top', self.topname)
        if self.cont != self.next:
            self.link(' | Cont', self.cont)
        self.link(' | Next', self.next, rel='next')

    def open_links(self, is_initial):
        self.write('<div class=Navigation>\n')
        if not is_initial:
            self.write(' <hr>\n')

    def close_links(self):
        self.write(' <hr>\n</div>\n')

    def output_charset(self):
        if self.charset:
            return '<meta http-equiv="Content-Type" content="text/html;charset='+self.charset+'">'
        else:
            return ''


class HTML5Node(HTMLNode):

    DOCTYPE = '<!DOCTYPE html>'

    def open_links(self, is_initial):
        self.write('<nav role=navigation>\n')
        if not is_initial:
            self.write(' <hr>\n')

    def close_links(self):
        self.write(' <hr>\n</nav>\n')

    def end_menu(self):
        self.write('</ul></nav>\n')
        if self.htmlhelp:
            self.htmlhelp.endmenu()

    def output_charset(self):
        if self.charset:
            return '<meta charset="'+self.charset+'">'
        else:
            return ''


class TexinfoParser:

    FN_ID_PATTERN = "(%(id)s)"
    FN_SOURCE_PATTERN = '<sup><a id=footnoteref%(id)s' \
                        ' href="#footnotetext%(id)s">' \
                        + FN_ID_PATTERN + '</a></sup>'
    FN_TARGET_PATTERN = '<li><a id=footnotetext%(id)s' \
                        ' href="#footnoteref%(id)s">' \
                        + FN_ID_PATTERN + '</a>\n%(text)s<p>\n'
    FN_HEADER = '\n<hr class=footrule>\n' \
                '<div><strong>Footnotes</strong></div>\n<ul>'
    FN_TRAILER = '</ul>'


    Node = HTMLNode

    # Initialize an instance
    def __init__(self):
        self.unknown = {}       # statistics about unknown @-commands
        self.filenames = {}     # Check for identical filenames
        self.debugging = 0      # larger values produce more output
        self.print_headers = 0  # always print headers?
        self.nodefp = None      # open file we're writing to
        self.nodelineno = 0     # Linenumber relative to node
        self.links = None       # Links from current node
        self.savetext = None    # If not None, save text head instead
        self.savestack = []     # If not None, save text head instead
        self.htmlhelp = None    # html help data
        self.dirname = 'tmp'    # directory where files are created
        self.includedir = '.'   # directory to search @include files
        self.nodename = ''      # name of current node
        self.topname = ''       # name of top node (first node seen)
        self.title = ''         # title of this whole Texinfo tree
        self.resetindex()       # Reset all indices
        self.contents = []      # Reset table of contents
        self.pregen_contents = None
        self.index_node = None
        self.has_modindex = False
        self.numbering = []     # Reset section numbering counters
        self.nofill = 0         # Normal operation: fill paragraphs
        self.values={'html': 1} # Names that should be parsed in ifset
        self.stackinfo = {}     # Keep track of state in the stack
        self.anchors = {}       # 
        self.last_table_row_was_head = 0 # 
        self.charset = "utf-8"  # 
        self.friendlies = None
        # XXX The following should be reset per node?!
        self.footnotes = []     # Reset list of footnotes
        self.itemarg = None     # Reset command used by @item
        self.itemnumber = None  # Reset number for @item in @enumerate
        self.itemindex = None   # Reset item index name
        self.node = None
        self.nodestack = []
        self.cont = 0
        self.includedepth = 0
        self.nodemenu = None

    # Set htmlhelp helper class
    def sethtmlhelp(self, htmlhelp):
        self.htmlhelp = htmlhelp
        for i in self.Node.gui_assets:
            self.htmlhelp.addimage(os.path.join(self.dirname, i))

    # Set (output) directory name
    def setdirname(self, dirname):
        self.dirname = dirname

    # Set include directory name
    def setincludedir(self, includedir):
        self.includedir = includedir

    # Parse the contents of an entire file
    def parse(self, fp):
        line = fp.readline()
        lineno = 1
        while line and (line[0] == '%' or blprog.match(line)):
            line = fp.readline()
            lineno = lineno + 1
        if hasattr(fp,"name"):
            fname = fp.name
        else:
            fname = "<unknown>"
        if line[:len(MAGIC)] != MAGIC:
            raise SyntaxError('file does not begin with %r' % (MAGIC,), (fname, lineno, 0, line))
        self.parserest(fp, lineno)

    # Parse the contents of a file, not expecting a MAGIC header
    def parserest(self, fp, initial_lineno):
        lineno = initial_lineno
        self.done = 0
        self.skip = 0
        self.stack = []
        accu = []
        while not self.done:
            line = fp.readline()
            self.nodelineno = self.nodelineno + 1
            if not line:
                if accu:
                    if not self.skip: self.process(accu)
                    accu = []
                if initial_lineno > 0:
                    print('*** EOF before @bye')
                break
            lineno = lineno + 1
            mo = cmprog.match(line)
            if mo:
                a, b = mo.span(1)
                cmd = line[a:b]
                if cmd in ('noindent', 'refill'):
                    accu.append(line)
                else:
                    if accu:
                        if not self.skip:
                            self.process(accu)
                        accu = []
                    self.command(line, mo)
            elif blprog.match(line) and \
                 'format' not in self.stack and \
                 'example' not in self.stack:
                if accu:
                    if not self.skip:
                        self.process(accu)
                        if self.nofill:
                            self.write('\n')
                        else:
                            self.write('<p>\n')
                        accu = []
            else:
                # Append the line including trailing \n!
                accu.append(line)
        #
        if self.skip:
            print('*** Still skipping at the end')
        if self.stack:
            print('*** Stack not empty at the end')
            print('***', self.stack)
        if self.includedepth == 0:
            while self.nodestack:
                self.nodestack[-1].finalize()
                self.nodestack[-1].flush()
                del self.nodestack[-1]

    # Start saving text in a buffer instead of writing it to a file
    def startsaving(self):
        if self.savetext is not None:
            self.savestack.append(self.savetext)
            # print '*** Recursively saving text, expect trouble'
        self.savetext = ''

    # Return the text saved so far and start writing to file again
    def collectsavings(self):
        savetext = self.savetext
        if len(self.savestack) > 0:
            self.savetext = self.savestack.pop()
        else:
            self.savetext = None
        return savetext or ''

    # Write text to file, or save it in a buffer, or ignore it
    def write(self, *args):
        try:
            text = ''.join(args)
        except:
            print(args)
            raise TypeError
        if self.savetext is not None:
            self.savetext = self.savetext + text
        elif self.nodefp:
            self.nodefp.write(text)
        elif self.node:
            self.node.write(text)

    # Complete the current node -- write footnotes and close file
    def endnode(self):
        if self.savetext is not None:
            print('*** Still saving text at end of node ' + self.node.name)
            dummy = self.collectsavings()
            self.write(dummy) 
        if self.nodemenu:
            self.write(self.nodemenu)
            self.nodemenu = None
        if self.footnotes:
            self.writefootnotes()
        if self.nodefp:
            if self.nodelineno > 20:
                self.write('<hr>\n')
                [name, next, prev, up] = self.nodelinks[:4]
                self.link('Next', next)
                self.link('Prev', prev)
                self.link('Up', up)
                if self.nodename != self.topname:
                    self.link('Top', self.topname)
                self.write('<hr>\n')
            self.write('</body>\n')
            self.nodefp.close()
            self.nodefp = None
        elif self.node:
            self.node.release_info = (self.copying.replace("<blockquote>", "").split("<p>", 1)[0]
                if self.copying else "")
            if not self.cont and \
               (not self.node.type or \
                (self.node.next and self.node.prev and self.node.up)):
                self.node.finalize()
                self.node.flush()
            else:
                self.nodestack.append(self.node)
            self.node = None
        self.nodename = ''

    def nodefile(self, nodename):
        if nodename in self.anchors.keys():
            return makefile(self.anchors[nodename].name) + "#" + filename_to_xmlid(fixfunnychars(nodename))
        else:
            return makefile(nodename)

    # Process a list of lines, expanding embedded @-commands
    # This mostly distinguishes between menus and normal text
    def process(self, accu):
        if self.debugging > 1:
            print('!'*self.debugging, 'process:', self.skip, self.stack, end=' ')
            if accu: print(accu[0][:30], end=' ')
            if accu[0][30:] or accu[1:]: print('...', end=' ')
            print()
        if self.inmenu():
            # XXX should be done differently
            for line in accu:
                mo = miprog.match(line)
                if not mo:
                    line = line.strip() + '\n'
                    self.expand(line)
                    continue
                bgn, end = mo.span(0)
                a, b = mo.span(1)
                c, d = mo.span(2)
                e, f = mo.span(3)
                g, h = mo.span(4)
                label = line[a:b]
                nodename = line[c:d]
                if nodename[0] == ':': nodename = label
                else: nodename = line[e:f]
                punct = line[g:h]
                nodefile = self.nodefile(nodename)
                self.write('  <li><a href="',
                           nodefile,
                           '">')
                if self.friendlies and nodename in self.friendlies:
                    # TODO this is fragile, assumes what is/isn't number we generated ourselves.
                    maybe_number = self.friendlies[nodename].split(None, 1)[0]
                    if not maybe_number.strip("0123456789."):
                        self.write(maybe_number, " ")
                self.expand(label)
                self.write('</a>', punct, '\n')
                if self.htmlhelp:
                    self.htmlhelp.menuitem(nodename)
                self.expand(line[end:])
        else:
            text = ''.join(accu)
            self.expand(text)

    # find 'menu' (we might be inside 'ifset' or 'ifclear')
    def inmenu(self):
        #if 'menu' in self.stack:
        #    print 'inmenu   :', self.skip, self.stack, self.stackinfo
        stack = self.stack
        while stack and stack[-1] in ('ifset','ifclear'):
            try:
                if self.stackinfo[len(stack)]:
                    return 0
            except KeyError:
                pass
            stack = stack[:-1]
        return (stack and stack[-1] == 'menu')

    # Write a string, expanding embedded @-commands
    def expand(self, text, wanthtml=True):
        stack = []
        i = 0
        n = len(text)
        while i < n:
            start = i
            mo = spprog.search(text, i)
            if mo:
                i = mo.start()
            else:
                self.write(text[start:])
                break
            self.write(text[start:i])
            c = text[i]
            i = i+1
            if c == '\n':
                self.write('\n')
                continue
            if c == '<' and wanthtml:
                self.write('&lt;')
                continue
            if c == '>' and wanthtml:
                self.write('&gt;')
                continue
            if c == '&' and wanthtml:
                self.write('&amp;')
                continue
            if c == '`' and text[i:] and text[i] == '`':
                self.write('“')
                i += 1
                continue
            if c == "'" and text[i:] and text[i] == "'":
                self.write('”')
                i += 1
                continue
            if c == "-" and text[i+1:] and text[i] == text[i+1] == "-":
                self.write('—')
                i += 2
                continue
            if c == "_" and text[i+64:] and not text[i:i+65].strip("_") and wanthtml:
                # Used in texi generated by Sphinx for a horizontal rule for some reason.
                self.write('<hr class=sphinxrule>')
                i += 65
                continue
            if c == '{':
                stack.append('')
                continue
            if c == '}':
                if not stack:
                    if not self.multi_paragraph_footnote:
                        print('*** Unmatched } in', self.nodename)
                        self.write('}')
                    else:
                        self.close_footnote()
                        self.multi_paragraph_footnote = False
                    continue
                cmd = stack.pop()
                if cmd == "w" and not wanthtml:
                    continue
                try:
                    method = getattr(self, 'close_' + cmd)
                except AttributeError:
                    self.unknown_close(cmd)
                    continue
                if cmd == "ref":
                    method(wanthtml)
                else:
                    method()
                continue
            if c != '@':
                if wanthtml:
                    # Cannot happen unless spprog is changed
                    raise SyntaxError('unexpected funny %r, char %d' % (c, i), ("?", 0, i, text))
                else:
                    self.write(c)
                    continue
            start = i
            while i < n and text[i] in string.ascii_letters: i = i+1
            if i == start:
                # @ plus non-letter: literal next character
                i = i+1
                c = text[start:i]
                if c == ':':
                    # `@:' means no extra space after
                    # preceding `.', `?', `!' or `:'
                    pass
                elif c == '*':
                    # GCC manual uses this to mean <br>
                    self.write("<br>" if wanthtml else "\n")
                else:
                    # `@.' means a sentence-ending period;
                    # `@@', `@{', `@}' quote `@', `{', `}'
                    self.write(c)
                continue
            cmd = text[start:i]
            if i < n and text[i] == '{':
                i = i+1
                stack.append(cmd)
                if cmd == "w" and not wanthtml:
                    continue
                try:
                    method = getattr(self, 'open_' + cmd)
                except AttributeError:
                    self.unknown_open(cmd)
                    continue
                method()
                continue
            try:
                method = getattr(self, 'handle_' + cmd)
            except AttributeError:
                self.unknown_handle(cmd)
                continue
            method()
        if stack:
            if stack == ["footnote"]:
                self.multi_paragraph_footnote = True
            else:
                print('*** Stack not empty at para:', stack, 'in', self.nodename)

    # --- Handle unknown embedded @-commands ---

    def unknown_open(self, cmd):
        print('*** No open func for @' + cmd + '{...}')
        cmd = cmd + '{'
        self.write('@', cmd)
        if cmd not in self.unknown:
            self.unknown[cmd] = 1
        else:
            self.unknown[cmd] = self.unknown[cmd] + 1

    def unknown_close(self, cmd):
        print('*** No close func for @' + cmd + '{...}')
        cmd = '}' + cmd
        self.write('}')
        if cmd not in self.unknown:
            self.unknown[cmd] = 1
        else:
            self.unknown[cmd] = self.unknown[cmd] + 1

    def unknown_handle(self, cmd):
        print('*** No handler for @' + cmd)
        self.write('@', cmd)
        if cmd not in self.unknown:
            self.unknown[cmd] = 1
        else:
            self.unknown[cmd] = self.unknown[cmd] + 1

    # XXX The following sections should be ordered as the texinfo docs

    # --- Embedded @-commands without {} argument list --

    def handle_noindent(self): pass

    def handle_refill(self): pass

    # --- Include file handling ---

    def do_include(self, args):
        file = args
        file = os.path.join(self.includedir, file)
        try:
            fp = open(file, 'r')
        except IOError as msg:
            print('*** Can\'t open include file', repr(file))
            return
        with fp:
            print('!'*self.debugging, '--> file', repr(file))
            save_done = self.done
            save_skip = self.skip
            save_stack = self.stack
            self.includedepth = self.includedepth + 1
            self.parserest(fp, 0)
            self.includedepth = self.includedepth - 1
        self.done = save_done
        self.skip = save_skip
        self.stack = save_stack
        print('!'*self.debugging, '<-- file', repr(file))

    # --- Special Insertions ---

    def open_dmn(self): pass
    def close_dmn(self): pass

    def open_dots(self): self.write('…')
    def close_dots(self): pass

    def handle_bullet(self): self.write('∙')
    def open_bullet(self): self.write('∙')
    def close_bullet(self): pass

    def open_TeX(self): self.write('Τ<sub>Ε</sub>Χ')
    def close_TeX(self): pass

    def handle_copyright(self): self.write("©")
    def open_copyright(self): self.write("©")
    def close_copyright(self): pass

    def open_minus(self): self.write('−')
    def close_minus(self): pass

    # --- Accents ---

    # rpyron 2002-05-07
    # I would like to do at least as well as makeinfo when
    # it is producing HTML output:
    #
    #   input               output
    #     @"o                 @"o                umlaut accent
    #     @'o                 'o                 acute accent
    #     @,{c}               @,{c}              cedilla accent
    #     @=o                 @=o                macron/overbar accent
    #     @^o                 @^o                circumflex accent
    #     @`o                 `o                 grave accent
    #     @~o                 @~o                tilde accent
    #     @dotaccent{o}       @dotaccent{o}      overdot accent
    #     @H{o}               @H{o}              long Hungarian umlaut
    #     @ringaccent{o}      @ringaccent{o}     ring accent
    #     @tieaccent{oo}      @tieaccent{oo}     tie-after accent
    #     @u{o}               @u{o}              breve accent
    #     @ubaraccent{o}      @ubaraccent{o}     underbar accent
    #     @udotaccent{o}      @udotaccent{o}     underdot accent
    #     @v{o}               @v{o}              hacek or check accent
    #     @exclamdown{}       &#161;             upside-down !
    #     @questiondown{}     &#191;             upside-down ?
    #     @aa{},@AA{}         &#229;,&#197;      a,A with circle
    #     @ae{},@AE{}         &#230;,&#198;      ae,AE ligatures
    #     @dotless{i}         @dotless{i}        dotless i
    #     @dotless{j}         @dotless{j}        dotless j
    #     @l{},@L{}           l/,L/              suppressed-L,l
    #     @o{},@O{}           &#248;,&#216;      O,o with slash
    #     @oe{},@OE{}         oe,OE              oe,OE ligatures
    #     @ss{}               &#223;             es-zet or sharp S

    def open_exclamdown(self): self.write('¡')   # upside-down !
    def close_exclamdown(self): pass
    def open_questiondown(self): self.write('¿') # upside-down ?
    def close_questiondown(self): pass
    def open_aa(self): self.write('å') # a with circle
    def close_aa(self): pass
    def open_AA(self): self.write('Å') # A with circle
    def close_AA(self): pass
    def open_ae(self): self.write('æ') # ae ligatures
    def close_ae(self): pass
    def open_AE(self): self.write('Æ') # AE ligatures
    def close_AE(self): pass
    def open_o(self): self.write('ø')  # o with slash
    def close_o(self): pass
    def open_O(self): self.write('Ø')  # O with slash
    def close_O(self): pass
    def open_ss(self): self.write('ß') # es-zet or sharp S
    def close_ss(self): pass
    def open_oe(self): self.write('œ')     # oe ligatures
    def close_oe(self): pass
    def open_OE(self): self.write('Œ')     # OE ligatures
    def close_OE(self): pass
    def open_l(self): self.write('ł')      # suppressed-l
    def close_l(self): pass
    def open_L(self): self.write('Ł')      # suppressed-L
    def close_L(self): pass
    def open_comma(self):
        if not self.inside_ref:
            self.write(',')   # comma in nodename
        else:
            self.write('\U0001FFFE') # comma in link text (escape as nonchar for the time being)
    def close_comma(self): pass
    def open_hashchar(self): self.write("#")
    def close_hashchar(self): pass
    def open_backslashchar(self): self.write("\\")
    def close_backslashchar(self): pass

    # --- Special Glyphs for Examples ---

    def open_result(self): self.write('=&gt;')
    def close_result(self): pass

    def open_expansion(self): self.write('==&gt;')
    def close_expansion(self): pass

    def open_print(self): self.write('-|')
    def close_print(self): pass

    def open_error(self): self.write('error--&gt;')
    def close_error(self): pass

    def open_equiv(self): self.write('==')
    def close_equiv(self): pass

    def open_point(self): self.write('-!-')
    def close_point(self): pass

    # --- Cross References ---

    inside_ref = False

    def open_pxref(self):
        self.write('see ')
        self.startsaving()
        self.inside_ref = True
    def close_pxref(self):
        self.makeref()

    def open_xref(self):
        self.write('See ')
        self.startsaving()
        self.inside_ref = True
    def close_xref(self):
        self.makeref()

    def open_ref(self):
        self.startsaving()
        self.inside_ref = True
    open_mylink = open_ref
    def close_ref(self, wanthtml=True):
        self.makeref(wanthtml)
    close_mylink = close_ref

    def open_inforef(self):
        self.write('See info file ')
        self.inside_ref = True
        self.startsaving()
    def close_inforef(self):
        self.inside_ref = False
        text = self.collectsavings()
        args = [s.strip() for s in text.split(',')]
        while len(args) < 3: args.append('')
        node = args[0].replace('\U0001FFFE', ',')
        file = args[2].replace('\U0001FFFE', ',')
        self.write('‘', file, '’, node ‘', node, '’')

    def makeref(self, wanthtml=True):
        # Node name, online link label, printed cross-reference label, manual file (if different),
        #   manual title (if different)
        self.inside_ref = False
        text = self.collectsavings()
        args = [s.strip() for s in text.split(',')]
        while len(args) < 5: args.append('')
        nodename = label = args[0].replace('\U0001FFFE', ',')
        if args[1]: label = args[1].replace('\U0001FFFE', ',')
        elif args[2]: label = args[2].replace('\U0001FFFE', ',')
        file = args[3].replace('\U0001FFFE', ',')
        title = args[4].replace('\U0001FFFE', ',')
        href = self.nodefile(nodename)
        if file:
            href = '../' + file + '/' + href
        if wanthtml:
            self.write('<a href="', href, '">', label, '</a>')
        else: # e.g. when we want a sort key for an index, or an entry in a CHM index
            self.write(label)

    # rpyron 2002-05-07  uref support
    def open_uref(self):
        self.startsaving()
    def close_uref(self):
        text = self.collectsavings()
        args = [s.strip() for s in text.split(',')]
        while len(args) < 2: args.append('')
        href = args[0]
        label = args[1]
        if not label: label = href
        self.write('<a href="', href, '">', label, '</a>')

    # rpyron 2002-05-07  image support
    # GNU makeinfo producing HTML output tries `filename.png'; if
    # that does not exist, it tries `filename.jpg'. If that does
    # not exist either, it complains. GNU makeinfo does not handle
    # GIF files; however, I include GIF support here because
    # MySQL documentation uses GIF files.

    def open_image(self):
        self.startsaving()
    def close_image(self):
        self.makeimage()
    def makeimage(self):
        text = self.collectsavings()
        args = [s.strip() for s in text.split(',')]
        while len(args) < 5: args.append('')
        filename = args[0]
        width    = args[1]
        height   = args[2]
        alt      = args[3] or filename
        # If it's given (and it doesn't have to be), ext is supposted to start with dot; however,
        #   Sphinx appears not to do this.
        ext      = args[4] if (not args[4]) or (args[4][0] == ".") else ("." + args[4])

        # The HTML output will have a reference to the image
        # that is relative to the HTML output directory,
        # which is what 'filename' gives us. However, we need
        # to find it relative to our own current directory
        _imagelocation = os.path.join(self.dirname, filename)

        if ext and os.path.exists(_imagelocation + ext):
            filename += ext
        elif os.path.exists(_imagelocation + '.png'):
            filename += '.png'
        elif os.path.exists(_imagelocation + '.jpg'):
            filename += '.jpg'
        elif os.path.exists(_imagelocation + '.gif'):
            filename += '.gif'
        else:
            print("*** Cannot find image " + _imagelocation)
        self.write('<img src="', filename, '"',                     \
                    width  and (' width="'  + width  + '"') or "",  \
                    height and (' height="' + height + '"') or "",  \
                    alt    and (' alt="'    + alt    + '"') or "",  \
                    '/>' )
        # The HHP file also needs the image path, including extension, relative to top directory.
        imagelocation = os.path.join(self.dirname, filename)
        if self.htmlhelp:
            self.htmlhelp.addimage(imagelocation)


    # --- Marking Words and Phrases ---

    # --- Other @xxx{...} commands ---

    def open_(self): pass # Used by {text enclosed in braces}
    def close_(self): pass

    open_asis = open_
    close_asis = close_

    def open_anchor(self):
        self.startsaving()
    def close_anchor(self):
        this_anchor = self.collectsavings()
        #print(f"*** anchor {this_anchor} is {self.node.name}")
        self.anchors[this_anchor] = self.node
        ffc = filename_to_xmlid(fixfunnychars(this_anchor))
        self.write(f'<a id="{ffc}"></a>')

    def open_indicateurl(self):
        self.startsaving()
    def close_indicateurl(self):
        url = self.collectsavings()
        self.write('<a href=\"' + url + '\">' + url + '</a>')

    def open_cite(self): self.write('<cite>')
    def close_cite(self): self.write('</cite>')

    def open_code(self): self.write('<code>')
    def close_code(self): self.write('</code>')

    def open_t(self): self.write('<tt>')
    def close_t(self): self.write('</tt>')

    def open_dfn(self): self.write('<dfn>')
    def close_dfn(self): self.write('</dfn>')

    def open_emph(self):
        if self.stack and self.stack[-1] == "deffn":
            self.startsaving()
        else:
            self.write('<em>')
    def close_emph(self):
        if self.stack and self.stack[-1] == "deffn":
            data = self.collectsavings()
            if match := refcounts_regex.match(data):
                self.write('<div class="refcount-info"><span class="label">Return value:</span> '
                           f'<span class="value">{match.group(1)}.</span></div>')
            else:
                self.write(f'<em>{data}</em>')
        else:
            self.write('</em>')

    def open_i(self): self.write('<i>')
    def close_i(self): self.write('</i>')

    def open_abbr(self):
        self.startsaving()
        if not hasattr(self, "abbrdict"):
            self.abbrdict = {}
    def close_abbr(self):
        # FIXME this will break if @comma{} is in the abbr itself.
        data = self.collectsavings()
        if "," in data:
            abbr, expan = data.split(",", 1)
            self.write(f'<abbr title="{expan}">{abbr}</abbr> ({expan})')
            self.abbrdict[abbr] = expan
        elif data in self.abbrdict:
            expan = self.abbrdict[data]
            self.write(f'<abbr title="{expan}">{data}</abbr>')
        else:
            print(f"*** ??? abbr {data!r}")
            self.write(f'<abbr>{data}</abbr>')

    multi_paragraph_footnote = False
    def open_footnote(self):
        # if self.savetext is not None:
        #       print '*** Recursive footnote -- expect weirdness'
        id = len(self.footnotes) + 1
        self.write(self.FN_SOURCE_PATTERN % {'id': repr(id)})
        self.startsaving()

    def close_footnote(self):
        id = len(self.footnotes) + 1
        self.footnotes.append((id, self.collectsavings()))

    def hyperlinks(self, text):
        if re_weburl.match(text.strip()):
            link = text.strip()
            # Fix any verbatim percent signs in the link if it doesn't contain escaping
            if urllib.parse.unquote(link) == link and "%" in link:
                link = urllib.parse.quote(link, ":/?&=#")
            return f'<a href="{link}" target="_blank">{text}</a>'
        return text

    def writefootnotes(self):
        self.write(self.FN_HEADER)
        for id, text in self.footnotes:
            self.write(self.FN_TARGET_PATTERN
                       % {'id': repr(id), 'text': self.hyperlinks(text)})
        self.write(self.FN_TRAILER)
        self.footnotes = []

    def open_file(self): self.write('<code>')
    def close_file(self): self.write('</code>')

    def open_kbd(self): self.write('<kbd>')
    def close_kbd(self): self.write('</kbd>')

    def open_key(self): self.write('<key>')
    def close_key(self): self.write('</key>')

    def open_r(self): self.write('<r>')
    def close_r(self): self.write('</r>')

    def open_samp(self): self.write('‘<samp>')
    def close_samp(self): self.write('</samp>’')

    def open_sc(self): self.write('<smallcaps>')
    def close_sc(self): self.write('</smallcaps>')

    def open_strong(self): self.write('<strong>')
    def close_strong(self): self.write('</strong>')

    def open_b(self): self.write('<b>')
    def close_b(self): self.write('</b>')

    def open_var(self): self.write('<var>')
    def close_var(self): self.write('</var>')

    def open_w(self): self.write('<span style="white-space: nowrap;">') # <nobr> / <nobreak>
    def close_w(self): self.write('</span>')

    def open_url(self): self.startsaving()
    def close_url(self):
        text = self.collectsavings()
        self.write('<a href="', text, '">', text, '</A>')

    def open_email(self): self.startsaving()
    def close_email(self):
        text = self.collectsavings()
        self.write('<a href="mailto:', text, '">', text, '</A>')

    open_titlefont = open_
    close_titlefont = close_

    def open_small(self): pass
    def close_small(self): pass

    def command(self, line, mo):
        a, b = mo.span(1)
        cmd = line[a:b]
        args = line[b:].strip()
        if self.debugging > 1:
            print('!'*self.debugging, 'command:', self.skip, self.stack, \
                  '@' + cmd, args)
        try:
            func = getattr(self, 'do_' + cmd)
        except AttributeError:
            try:
                func = getattr(self, 'bgn_' + cmd)
            except AttributeError:
                # don't complain if we are skipping anyway
                if not self.skip:
                    self.unknown_cmd(cmd, args)
                return
            self.stack.append(cmd)
            func(args)
            return
        if not self.skip or cmd == 'end':
            func(args)

    def unknown_cmd(self, cmd, args):
        print('*** unknown', '@' + cmd, args)
        if cmd not in self.unknown:
            self.unknown[cmd] = 1
        else:
            self.unknown[cmd] = self.unknown[cmd] + 1

    def do_end(self, args):
        words = args.split()
        if not words:
            print('*** @end w/o args')
        else:
            cmd = words[0]
            if not self.stack or self.stack[-1] != cmd:
                print('*** @end', cmd, 'unexpected')
            else:
                del self.stack[-1]
            try:
                func = getattr(self, 'end_' + cmd)
            except AttributeError:
                self.unknown_end(cmd)
                return
            func()

    def unknown_end(self, cmd):
        cmd = 'end ' + cmd
        print('*** unknown', '@' + cmd)
        if cmd not in self.unknown:
            self.unknown[cmd] = 1
        else:
            self.unknown[cmd] = self.unknown[cmd] + 1

    # --- Comments ---

    def do_comment(self, args): pass
    do_c = do_comment

    # --- Conditional processing ---

    def bgn_ifinfo(self, args): pass
    def end_ifinfo(self): pass

    def bgn_iftex(self, args): self.skip = self.skip + 1
    def end_iftex(self): self.skip = self.skip - 1

    # used by Sphinx
    def bgn_ifnottex(self, args): pass
    def end_ifnottex(self): pass

    def bgn_ignore(self, args): self.skip = self.skip + 1
    def end_ignore(self): self.skip = self.skip - 1

    def bgn_tex(self, args): self.skip = self.skip + 1
    def end_tex(self): self.skip = self.skip - 1

    def do_set(self, args):
        fields = args.split(' ')
        key = fields[0]
        if len(fields) == 1:
            value = 1
        else:
            value = ' '.join(fields[1:])
        self.values[key] = value

    def do_clear(self, args):
        self.values[args] = None

    def bgn_ifset(self, args):
        if args not in self.values or self.values[args] is None:
            self.skip = self.skip + 1
            self.stackinfo[len(self.stack)] = 1
        else:
            self.stackinfo[len(self.stack)] = 0
    def end_ifset(self):
        try:
            if self.stackinfo[len(self.stack) + 1]:
                self.skip = self.skip - 1
            del self.stackinfo[len(self.stack) + 1]
        except KeyError:
            print('*** end_ifset: KeyError :', len(self.stack) + 1)

    def bgn_ifclear(self, args):
        if args in self.values and self.values[args] is not None:
            self.skip = self.skip + 1
            self.stackinfo[len(self.stack)] = 1
        else:
            self.stackinfo[len(self.stack)] = 0
    def end_ifclear(self):
        try:
            if self.stackinfo[len(self.stack) + 1]:
                self.skip = self.skip - 1
            del self.stackinfo[len(self.stack) + 1]
        except KeyError:
            print('*** end_ifclear: KeyError :', len(self.stack) + 1)

    def open_value(self):
        self.startsaving()

    def close_value(self):
        key = self.collectsavings()
        if key in self.values:
            self.write(self.values[key])
        else:
            print('*** Undefined value: ', key)

    # --- Beginning a file ---

    do_finalout = do_comment
    do_setchapternewpage = do_comment
    do_setfilename = do_comment

    def do_settitle(self, args):
        self.startsaving()
        self.expand(args)
        self.title = self.collectsavings()
        self.htmlhelp.settitle(self.title)
    def do_parskip(self, args): pass
    def do_afourlatex(self, args): pass
    def do_documentencoding(self, args):
        pass # handled by procure_encoding(...) before parsing even begins.

    # --- Ending a file ---

    def do_bye(self, args):
        self.endnode()
        self.done = 1

    # --- Title page ---

    title_page = None
    def bgn_titlepage(self, args):
        self.startsaving()
    def end_titlepage(self):
        self.title_page = self.collectsavings()
    def do_shorttitlepage(self, args): pass

    copying = None
    copying_inserted = False
    def bgn_copying(self, args):
        self.startsaving()
    def end_copying(self):
        self.copying = self.collectsavings()
    def do_insertcopying(self, args):
        if self.copying is not None and not self.copying_inserted:
            self.write(self.copying)
            # Don't insert it twice when merging top node with title page
            self.copying_inserted = True

    def do_center(self, args):
        # Actually not used outside title page...
        self.write('<div style="text-align: center;">') # <center>
        self.expand(args)
        self.write('</div>\n')
    def do_title(self, args):
        self.write('<div style="text-align: center;"><h1>') # <center>
        self.expand(args)
        self.write('</h1></div>\n')
    def do_subtitle(self, args):
        self.write('<div style="text-align: center;"><h2>') # <center>
        self.expand(args)
        self.write('</h2></div>\n')
    def bgn_titlefont(self, args):
        self.write('<h1>')
    def end_titlefont(self):
        self.write('</h1>\n')
    def bgn_subtitlefont(self, args):
        self.write('<h2>')
    def end_subtitlefont(self):
        self.write('</h2>\n')
    do_author = do_center

    do_vskip = do_comment
    do_vfill = do_comment
    do_smallbook = do_comment

    do_paragraphindent = do_comment
    do_setchapternewpage = do_comment
    do_headings = do_comment
    do_footnotestyle = do_comment

    do_evenheading = do_comment
    do_evenfooting = do_comment
    do_oddheading = do_comment
    do_oddfooting = do_comment
    do_everyheading = do_comment
    do_everyfooting = do_comment

    # --- Nodes ---

    def do_node(self, args, *, is_toc=False):
        parts = [s.strip() for s in args.split(',')]
        while len(parts) < 4: parts.append('')
        [name, next, prev, up] = parts[:4]
        file = self.dirname + '/' + makefile(name)
        tocnode = None
        if not self.topname and not is_toc:
            self.topname = name
            if self.node:
                tocnode = self.node
                tocnode.up = name
        self.endnode()
        self.nodelineno = 0
        self.nodelinks = parts
        if file in self.filenames:
            print('*** Filename already in use: ', file)
        else:
            if self.debugging: print('!'*self.debugging, '--- writing', file)
        self.filenames[file] = 1
        # self.nodefp = open(file, 'w')
        self.nodename = name
        title = name
        if self.title: title = title + ' — ' + self.title
        self.node = self.Node(self.dirname, self.nodename, self.topname,
                              title, next, prev, up, self.charset, 
                              self.friendlies, self.index_node, self.has_modindex)
        if self.cont and self.nodestack and not tocnode:
            self.nodestack[-1].cont = self.nodename
            self.node.rcont = self.nodestack[-1].name
        if not is_toc:
            self.htmlhelp.addnode(name, next, prev, up, file, self.node.clean_name(name))
            if tocnode:
                print("###", self.dirname + '/' + makefile(tocnode.name))
                self.htmlhelp.addnode(tocnode.name, '', '', name,
                        self.dirname + '/' + makefile(tocnode.name),
                        self.node.clean_name(tocnode.name))
                # FIXME: massive kludge
                self.htmlhelp.current = ''
                self.htmlhelp.menudict[''] = []

    def link(self, label, nodename):
        if nodename:
            if nodename.lower() == '(dir)':
                addr = '../dir.html'
            else:
                addr = self.nodefile(nodename)
            self.write(label, ': <a href="', addr, '" type="',
                       label, '">', nodename, '</a>  \n')

    # --- Sectioning commands ---

    def popstack(self, type):
        if (self.node):
            self.node.type = type
            while self.nodestack:
                if self.nodestack[-1].type > type:
                    self.nodestack[-1].finalize()
                    self.nodestack[-1].flush()
                    del self.nodestack[-1]
                elif self.nodestack[-1].type == type:
                    if not self.nodestack[-1].next:
                        self.nodestack[-1].next = self.node.name
                    if not self.node.prev:
                        self.node.prev = self.nodestack[-1].name
                    self.nodestack[-1].finalize()
                    self.nodestack[-1].flush()
                    del self.nodestack[-1]
                else:
                    if type > 1 and not self.node.up:
                        self.node.up = self.nodestack[-1].name
                    break

    def do_chapter(self, args):
        self.heading('h1', args, 0)
        self.popstack(1)

    def do_unnumbered(self, args):
        self.heading('h1', args, -1)
        self.popstack(1)
    def do_appendix(self, args):
        self.heading('h1', args, -1)
        self.popstack(1)
    def do_top(self, args):
        if not self.title_page:
            self.heading('h1', args, -1)
        else:
            self.write(self.title_page)
    def do_chapheading(self, args):
        self.heading('h1', args, -1)
    def do_majorheading(self, args):
        self.heading('h1', args, -1)

    def do_section(self, args):
        self.heading('h1', args, 1)
        self.popstack(2)

    def do_unnumberedsec(self, args):
        self.heading('h1', args, -1)
        self.popstack(2)
    def do_appendixsec(self, args):
        self.heading('h1', args, -1)
        self.popstack(2)
    do_appendixsection = do_appendixsec
    def do_heading(self, args):
        self.heading('h1', args, -1)

    def do_subsection(self, args):
        self.heading('h2', args, 2)
        self.popstack(3)
    def do_unnumberedsubsec(self, args):
        self.heading('h2', args, -1)
        self.popstack(3)
    def do_appendixsubsec(self, args):
        self.heading('h2', args, -1)
        self.popstack(3)
    def do_subheading(self, args):
        self.heading('h2', args, -1)

    def do_subsubsection(self, args):
        self.heading('h3', args, 3)
        self.popstack(4)
    def do_unnumberedsubsubsec(self, args):
        self.heading('h3', args, -1)
        self.popstack(4)
    def do_appendixsubsubsec(self, args):
        self.heading('h3', args, -1)
        self.popstack(4)
    def do_subsubheading(self, args):
        self.heading('h3', args, -1)

    def heading(self, type, args, level):
        heading_text = args
        if level >= 0:
            while len(self.numbering) <= level:
                self.numbering.append(0)
            del self.numbering[level+1:]
            self.numbering[level] = self.numbering[level] + 1
            if self.friendlies and self.nodename in self.friendlies:
                x = self.friendlies[self.nodename].split(None, 1)[0]
                if x.strip("0123456789."): # FIXME kludge (though this shouldn't happen anyway)
                    x = ''
            else:
                x = ''
                for i in self.numbering:
                    x = x + repr(i) + '.'
            heading_text = x + ' ' + args
            self.contents.append((level, heading_text, self.nodename))
        self.write('<', type, '>')
        self.expand(heading_text)
        self.write('</', type, '>\n<p>')
        if self.debugging or self.print_headers:
            print('---', args)

    has_contents = False
    def do_contents(self, args):
        self.do_node('Table of Contents', is_toc=True)
        self.listcontents('Table of Contents', 999)
        self.has_contents = True

    def do_shortcontents(self, args):
        pass
        # self.listcontents('Short Contents', 0)
    do_summarycontents = do_shortcontents

    def listcontents(self, title, maxlevel):
        self.write('<h1>', title, '</h1>\n<ul>\n')
        prevlevels = [0]
        for level, title, node in self.pregen_contents or self.contents:
            if level > maxlevel:
                continue
            if level > prevlevels[-1]:
                # can only advance one level at a time
                self.write('  '*prevlevels[-1], '<ul>\n')
                prevlevels.append(level)
            elif level < prevlevels[-1]:
                # might drop back multiple levels
                while level < prevlevels[-1] and prevlevels[1:]:
                    del prevlevels[-1]
                    self.write('  '*prevlevels[-1],
                               '</ul>\n')
            self.write('  '*level, '<li> <a href="',
                       self.nodefile(node), '">')
            self.expand(title)
            self.write('</a>\n')
        self.write('</ul>\n' * len(prevlevels))

    # --- Page lay-out ---

    # These commands are only meaningful in printed text

    def do_page(self, args): pass

    def do_need(self, args): pass

    def bgn_group(self, args): pass
    def end_group(self): pass

    # --- Line lay-out ---

    def do_sp(self, args):
        if self.nofill:
            self.write('\n')
        else:
            self.write('<p>\n')

    def do_hline(self, args):
        self.write('<hr>')

    # --- Function and variable definitions ---

    def bgn_deffn(self, args):
        self.write('<dl>')
        self.do_deffnx(args)

    def end_deffn(self):
        self.write('</dl><p>\n')

    def do_deffnx(self, args):
        # TODO almost the same code is repeated several times in the do_def* functions below
        # TODO probably shouldn't be using <b> and <tt> elements for this
        self.write('<dt>')
        words = splitwords(args, 2)
        [category, name], rest = words[:2], words[2:]
        cat = category.strip('{}').lower()
        # These special cases are geared mainly to Sphinx's output.
        if cat not in ('function', 'data', 'c type', 'c function', 'c variable', 'c member',
                       'c macro'):
            #print("###", cat)
            self.open_b()
            self.write('<span class="typelabel">', cat, '</span> ')
            self.close_b()
        if cat != "data":
            while rest and not (rest[0].startswith("(")):
                # `name` is actually part of the variable or return type here, not yet the name.
                self.expand(name)
                self.write(' ')
                name, rest = rest[0], rest[1:]
        else:
            name += ' ' + ' '.join(rest)
            rest = []
        self.open_b()
        self.open_t()
        self.expand(name)
        self.close_t()
        self.close_b()
        self.expand(' '.join(makevar(word) for word in rest))
        #self.expand(' -- ' + category)
        self.write('\n<dd>')
        self.index('fn', name)

    def bgn_defun(self, args): self.bgn_deffn('Function ' + args)
    end_defun = end_deffn
    def do_defunx(self, args): self.do_deffnx('Function ' + args)

    def bgn_defmac(self, args): self.bgn_deffn('Macro ' + args)
    end_defmac = end_deffn
    def do_defmacx(self, args): self.do_deffnx('Macro ' + args)

    def bgn_defspec(self, args): self.bgn_deffn('{Special Form} ' + args)
    end_defspec = end_deffn
    def do_defspecx(self, args): self.do_deffnx('{Special Form} ' + args)

    def bgn_defvr(self, args):
        self.write('<dl>')
        self.do_defvrx(args)

    end_defvr = end_deffn

    def do_defvrx(self, args):
        self.write('<dt>')
        words = splitwords(args, 2)
        [category, name], rest = words[:2], words[2:]
        self.expand('@code{%s}' % name)
        # If there are too many arguments, show them
        for word in rest: self.expand(' ' + word)
        #self.expand(' -- ' + category)
        self.write('\n<dd>')
        self.index('vr', name)

    def bgn_defvar(self, args): self.bgn_defvr('Variable ' + args)
    end_defvar = end_defvr
    def do_defvarx(self, args): self.do_defvrx('Variable ' + args)

    def bgn_defopt(self, args): self.bgn_defvr('{User Option} ' + args)
    end_defopt = end_defvr
    def do_defoptx(self, args): self.do_defvrx('{User Option} ' + args)

    # --- Ditto for typed languages ---

    def bgn_deftypefn(self, args):
        self.write('<dl>')
        self.do_deftypefnx(args)

    end_deftypefn = end_deffn

    def do_deftypefnx(self, args):
        self.write('<dt>')
        words = splitwords(args, 3)
        [category, datatype, name], rest = words[:3], words[3:]
        self.expand('@code{%s} @b{%s}' % (datatype, name))
        for word in rest: self.expand(' ' + makevar(word))
        #self.expand(' -- ' + category)
        self.write('\n<dd>')
        self.index('fn', name)


    def bgn_deftypefun(self, args): self.bgn_deftypefn('Function ' + args)
    end_deftypefun = end_deftypefn
    def do_deftypefunx(self, args): self.do_deftypefnx('Function ' + args)

    def bgn_deftypevr(self, args):
        self.write('<dl>')
        self.do_deftypevrx(args)

    end_deftypevr = end_deftypefn

    def do_deftypevrx(self, args):
        self.write('<dt>')
        words = splitwords(args, 3)
        [category, datatype, name], rest = words[:3], words[3:]
        self.expand('@code{%s} @b{%s}' % (datatype, name))
        # If there are too many arguments, show them
        for word in rest: self.expand(' ' + word)
        #self.expand(' -- ' + category)
        self.write('\n<dd>')
        self.index('fn', name)

    def bgn_deftypevar(self, args):
        self.bgn_deftypevr('Variable ' + args)
    end_deftypevar = end_deftypevr
    def do_deftypevarx(self, args):
        self.do_deftypevrx('Variable ' + args)

    # --- Ditto for object-oriented languages ---

    def bgn_defcv(self, args):
        self.write('<dl>')
        self.do_defcvx(args)

    end_defcv = end_deftypevr

    def do_defcvx(self, args):
        self.write('<dt>')
        words = splitwords(args, 3)
        [category, classname, name], rest = words[:3], words[3:]
        self.expand('@b{%s}' % name)
        # If there are too many arguments, show them
        for word in rest: self.expand(' ' + word)
        #self.expand(' -- %s of @code{%s}' % (category, classname))
        self.write('\n<dd>')
        self.index('vr', '%s @r{on %s}' % (name, classname))

    def bgn_defivar(self, args):
        self.bgn_defcv('{Instance Variable} ' + args)
    end_defivar = end_defcv
    def do_defivarx(self, args):
        self.do_defcvx('{Instance Variable} ' + args)

    def bgn_defop(self, args):
        self.write('<dl>')
        self.do_defopx(args)

    end_defop = end_defcv

    def do_defopx(self, args):
        self.write('<dt>')
        words = splitwords(args, 3)
        [category, classname, name], rest = words[:3], words[3:]
        self.expand('@b{%s}' % name)
        for word in rest: self.expand(' ' + makevar(word))
        #self.expand(' -- %s of @code{%s}' % (category, classname))
        self.write('\n<dd>')
        self.index('fn', '%s @r{on %s}' % (name, classname))

    def bgn_defmethod(self, args):
        self.bgn_defop('Method ' + args)
    end_defmethod = end_defop
    def do_defmethodx(self, args):
        self.do_defopx('Method ' + args)

    # --- Ditto for data types ---

    def bgn_deftp(self, args):
        self.write('<dl>')
        self.do_deftpx(args)

    end_deftp = end_defcv

    def do_deftpx(self, args):
        self.write('<dt>')
        words = splitwords(args, 2)
        [category, name], rest = words[:2], words[2:]
        self.expand('@b{%s}' % name)
        for word in rest: self.expand(' ' + word)
        #self.expand(' -- ' + category)
        self.write('\n<dd>')
        self.index('tp', name)

    # --- Making Lists and Tables

    def bgn_enumerate(self, args):
        if not args:
            self.write('<ol>\n')
            self.stackinfo[len(self.stack)] = '</ol><p>\n'
        else:
            self.itemnumber = args
            self.write('<ul>\n')
            self.stackinfo[len(self.stack)] = '</ul><p>\n'
    def end_enumerate(self):
        self.itemnumber = None
        self.write(self.stackinfo[len(self.stack) + 1])
        del self.stackinfo[len(self.stack) + 1]

    def bgn_itemize(self, args):
        self.itemarg = args
        if args.strip() == "*": # Sphinx generates it like this for some reason.
            self.itemarg = ""
        self.write('<ul>\n')
    def end_itemize(self):
        self.itemarg = None
        self.write('</ul><p>\n')

    def bgn_table(self, args):
        self.itemarg = args
        self.write('<dl>\n')
    def end_table(self):
        self.itemarg = None
        self.write('</dl><p>\n')

    def bgn_ftable(self, args):
        self.itemindex = 'fn'
        self.bgn_table(args)
    def end_ftable(self):
        self.itemindex = None
        self.end_table()

    def bgn_vtable(self, args):
        self.itemindex = 'vr'
        self.bgn_table(args)
    def end_vtable(self):
        self.itemindex = None
        self.end_table()

    def do_item(self, args):
        if self.itemindex: self.index(self.itemindex, args)
        if self.itemarg:
            if self.itemarg[0] == '@' and self.itemarg[1] and \
                            self.itemarg[1] in string.ascii_letters:
                args = self.itemarg + '{' + args + '}'
            else:
                # some other character, e.g. '-'
                args = self.itemarg + ' ' + args
        if self.itemnumber is not None:
            args = self.itemnumber + '. ' + args
            self.itemnumber = increment(self.itemnumber)
        if self.stack and self.stack[-1] == 'table':
            self.write('<dt>')
            self.expand(args)
            self.write('\n<dd>')
        elif self.stack and self.stack[-1] == 'multitable':
            if self.last_table_row_was_head:
                self.write('</th>\n</tr>\n')
            else:
                self.write('</td>\n</tr>\n')
            self.write('<tr><td>')
            self.expand(args)
        else:
            self.write('<li>')
            self.expand(args)
            self.write('  ')
        self.last_table_row_was_head = 0
    do_itemx = do_item # XXX Should suppress leading blank line
    
    def do_headitem(self, args):
        if self.itemindex: self.index(self.itemindex, args)
        if self.itemarg:
            if self.itemarg[0] == '@' and self.itemarg[1] and \
                            self.itemarg[1] in string.ascii_letters:
                args = self.itemarg + '{' + args + '}'
            else:
                # some other character, e.g. '-'
                args = self.itemarg + ' ' + args
        if self.itemnumber != None:
            args = self.itemnumber + '. ' + args
            self.itemnumber = increment(self.itemnumber)
        self.write('<tr><th>')
        self.last_table_row_was_head = 1
        self.expand(args)

    # rpyron 2002-05-07  multitable support
    def bgn_multitable(self, args):
        self.itemarg = None     # should be handled by columnfractions
        self.write('</p><table border="" class=realtable>\n')
    def end_multitable(self):
        self.itemarg = None
        self.write('</table>\n<p>')
    def handle_columnfractions(self):
        # It would be better to handle this, but for now it's in the way...
        self.itemarg = None
    def handle_tab(self):
        if self.last_table_row_was_head:
            self.write('</th>\n    <th>')
        else:
            self.write('</td>\n    <td>')
    def do_tab(self, args):
        self.handle_tab()

    # --- Enumerations, displays, quotations ---
    # XXX Most of these should increase the indentation somehow

    def bgn_quotation(self, args):
        if not self.in_cartouche:
            self.write('<blockquote><div>')
        else:
            class_ = "warning"
            if args.strip() == "Note":
                class_ = "note"
            self.write(f'<div class={class_}><b class=label>')
            self.expand(args)
            self.write(':</b><blockquote><div>')
        self.bgn_indentedblock("")
    def end_quotation(self):
        self.end_indentedblock()
        if not self.in_cartouche:
            self.write('</div></blockquote><p>\n')
        else:
            self.write('</div></blockquote></div><p>\n')

    def bgn_indentedblock(self, args):
        self.write('<blockquote><div>')
    def end_indentedblock(self):
        self.write('</div></blockquote><p>\n')

    in_cartouche = False
    def bgn_cartouche(self, args): self.in_cartouche = True
    def end_cartouche(self): self.in_cartouche = False # Assuming that they don't nest.

    def bgn_example(self, args):
        self.nofill = self.nofill + 1
        self.write('<div class="verbatim"><pre>')
    def end_example(self):
        self.write('</pre></div><p>\n')
        self.nofill = self.nofill - 1

    bgn_lisp = bgn_example # Synonym when contents are executable lisp code
    end_lisp = end_example

    bgn_smallexample = bgn_example # XXX Should use smaller font
    end_smallexample = end_example

    bgn_smalllisp = bgn_lisp # Ditto
    end_smalllisp = end_lisp
    
    bgn_smallindentedblock = bgn_indentedblock
    end_smallindentedblock = end_indentedblock

    bgn_display = bgn_example
    end_display = end_example

    bgn_format = bgn_display
    end_format = end_display

    def do_exdent(self, args): self.expand(args + '\n')
    # XXX Should really mess with indentation

    def bgn_flushleft(self, args):
        self.nofill = self.nofill + 1
        self.write('<pre>\n')
    def end_flushleft(self):
        self.write('</pre><p>\n')
        self.nofill = self.nofill - 1

    def bgn_flushright(self, args):
        self.nofill = self.nofill + 1
        self.write('<address>\n')
    def end_flushright(self):
        self.write('</address>\n')
        self.nofill = self.nofill - 1

    def bgn_menu(self, args):
        if not self.nodemenu:
            self.startsaving()
        if self.topname != self.nodename:
            self.write('<div><hr><strong>Subsections</strong><ul>\n')
        else:
            self.write('<div><hr><ul>\n')
            if self.has_contents:
                self.write('  <li><a href="', self.nodefile('Table of Contents'),
                           '">Table of Contents</a>')
        if self.htmlhelp:
            self.htmlhelp.beginmenu()
    def end_menu(self):
        self.write('</ul></div>\n')
        if not self.nodemenu:
            self.nodemenu = self.collectsavings()
        if self.htmlhelp:
            self.htmlhelp.endmenu()

    def bgn_detailmenu(self, args):
        self.startsaving()
    def end_detailmenu(self):
        # Don't include the detail menu; you're better off generating one yourself than trying
        #   to work with a TeXinfo file's poor approach to a hierachal docmap in flat menus.
        #   See: the handler for @contents.
        self.collectsavings()
 
    def open_gcctabopt(self): self.open_code()
    def close_gcctabopt(self): self.close_code()
    def open_command(self): self.open_code()
    def close_command(self): self.close_code()
    def open_option(self): self.open_code()
    def close_option(self): self.close_code()
    def open_env(self): self.open_code()
    def close_env(self): self.close_code()
    def handle_gol(self):
        self.write('<br>') # GCC manual uses this to mean <br>


    # --- Indices ---

    def resetindex(self):
        self.noncodeindices = ['cp', 'op']
        self.indextitle = {}
        self.indextitle['cp'] = 'Concept'
        self.indextitle['fn'] = 'Function'
        self.indextitle['ky'] = 'Keyword'
        self.indextitle['pg'] = 'Program'
        self.indextitle['tp'] = 'Type'
        self.indextitle['vr'] = 'Variable'
        self.indextitle['op'] = 'Option'
        #
        self.whichindex = {}
        for name in self.indextitle:
            self.whichindex[name] = set()

    def user_index(self, name, args):
        if name in self.whichindex:
            self.index(name, args)
        else:
            print('*** No index named', repr(name))

    def do_cindex(self, args): self.index('cp', args)
    def do_findex(self, args): self.index('fn', args)
    def do_kindex(self, args): self.index('ky', args)
    def do_pindex(self, args): self.index('pg', args)
    def do_tindex(self, args): self.index('tp', args)
    def do_vindex(self, args): self.index('vr', args)
    def do_opindex(self, args): self.index('op', args)

    def do_defindex(self, indtype):
        self.__dict__["do_" + indtype + "index"] = lambda args: self.index(indtype, args)
        self.whichindex[indtype] = set()
        self.indextitle[indtype] = indtype + " instance"

    def cleanindexkey(self, key):
        self.startsaving()
        self.expand(key, wanthtml=False)
        return self.collectsavings()

    def index(self, name, args):
        self.whichindex[name].add((args, self.nodename))
        if self.htmlhelp:
            self.htmlhelp.index(self.cleanindexkey(args), self.nodename)

    def do_synindex(self, args):
        words = args.split()
        if len(words) != 2:
            print('*** bad @synindex', args)
            return
        [old, new] = words
        if old not in self.whichindex or \
                  new not in self.whichindex:
            print('*** bad key(s) in @synindex', args)
            return
        if old != new and \
                  self.whichindex[old] is not self.whichindex[new]:
            inew = self.whichindex[new]
            inew[len(inew):] = self.whichindex[old]
            self.whichindex[old] = inew
    do_syncodeindex = do_synindex # XXX Should use code font

    def do_printindex(self, args):
        words = args.split()
        for name in words:
            if name in self.whichindex:
                self.prindex(name)
            else:
                print('*** No index named', repr(name))

    def prindex(self, name):
        iscodeindex = (name not in self.noncodeindices)
        index = self.whichindex[name]
        if not index: return
        if self.debugging:
            print('!'*self.debugging, '--- Generating', \
                  self.indextitle[name], 'index')
        #  The node already provides a title
        index1 = []
        junkprog = re.compile('(?i)^(@[a-z]+)?{')
        locale.setlocale(locale.LC_COLLATE, locale.getlocale())
        for key, node in index:
            sortkey = locale.strxfrm(self.cleanindexkey(key))
            index1.append((sortkey, key, node))
        index.clear()
        index1.sort()
        self.write('<dl>\n')
        prevkey = prevnode = None
        for sortkey, key, node in index1:
            if (key, node) == (prevkey, prevnode):
                continue
            if self.debugging > 1: print('!'*self.debugging, key, ':', node)
            self.write('<dt>')
            if iscodeindex: key = '@code{' + key + '}'
            if key != prevkey:
                self.expand(key)
            self.write('\n<dd><a href="%s">%s</a>\n' % (self.nodefile(node), 
                self.node.clean_name(node)))
            prevkey, prevnode = key, node
        self.write('</dl><p>\n')

    # --- Final error reports ---

    def report(self):
        if self.unknown:
            print('--- Unrecognized commands ---')
            cmds = sorted(self.unknown.keys())
            for cmd in cmds:
                print(cmd.ljust(20), self.unknown[cmd])


class TexinfoParserHTML5(TexinfoParser):

    FN_ID_PATTERN = "(%(id)s)"
    FN_SOURCE_PATTERN = '<sup><a id=footnoteref%(id)s' \
                        ' href="#footnotetext%(id)s">' \
                        + FN_ID_PATTERN + '</a></sup>'
    FN_TARGET_PATTERN = '<li><a id=footnotetext%(id)s' \
                        ' href="#footnoteref%(id)s">' \
                        + FN_ID_PATTERN + '</a>\n%(text)s<p>\n'
    FN_HEADER = '\n<hr class=footrule>\n' \
                '<footer><figure><figcaption>Footnotes</figcaption><ul>\n'
    FN_TRAILER = '</ul></figure></footer>'
    Node = HTML5Node

    def bgn_menu(self, args):
        if self.topname != self.nodename:
            self.write('<nav role=navigation><hr class=menurule><figure><figcaption>Subsections</figcaption><ul>\n')
        else:
            self.write('<nav role=navigation><hr class=menurule><ul>\n')
            if self.has_contents:
                self.write('  <li><a href="', self.nodefile('Table of Contents'),
                           '">Table of Contents</a>')
        if self.htmlhelp:
            self.htmlhelp.beginmenu()

    def end_menu(self):
        if self.topname != self.nodename:
            self.write('</ul></figure></nav>\n')
        else:
            self.write('</ul></nav>\n')
        if self.htmlhelp:
            self.htmlhelp.endmenu()

    def bgn_itemize(self, args):
        randid = "ul" + secrets.token_hex()
        #self.write(f'<style type="text/css">#{randid} li::marker{{content:{args!r};}}</style>\n')
        self.write(f"""<style type="text/css">#{randid} {{list-style:url("data:image/svg+xml,<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 32 32' width='20' height='20'><text x='16' y='32' style='font-size: 32px;' text-anchor='middle'>""")
        self.expand(args if len(args) == 1 or args[0] == '@' else '@' + args)
        self.write('</text></svg>");}</style>\n')
        self.itemarg = ""
        self.write(f'<ul id="{randid}">\n')

    def end_itemize(self):
        self.itemarg = None
        self.write('</ul>\n')


# rpyron 2002-05-07
class HTMLHelp:
    """
    This class encapsulates support for HTML Help. Node names,
    file names, menu items, index items, and image file names are
    accumulated until a call to finalize(). At that time, three
    output files are created in the current directory:

        `helpbase`.hhp  is a HTML Help Workshop project file.
                        It contains various information, some of
                        which I do not understand; I just copied
                        the default project info from a fresh
                        installation.
        `helpbase`.hhc  is the Contents file for the project.
        `helpbase`.hhk  is the Index file for the project.

    When these files are used as input to HTML Help Workshop,
    the resulting file will be named:

        `helpbase`.chm

    If none of the defaults in `helpbase`.hhp are changed,
    the .CHM file will have Contents, Index, Search, and
    Favorites tabs.
    """

    codeprog = re.compile('@code{(.*?)}')

    def __init__(self, helpbase, dirname, do_build=False):
        self.helpbase    = helpbase
        self.dirname     = dirname
        self.projectfile = None
        self.contentfile = None
        self.indexfile   = None
        self.nodelist    = []
        self.nodenames   = {}         # nodename : index
        self.nodeindex   = {}
        self.filenames   = {}         # filename : filename
        self.indexlist   = set()      # (args,nodename) == (key,location)
        self.current     = ''
        self.menudict    = {}
        self.dumped      = {}
        self.title       = None
        self.do_build    = do_build


    def addnode(self, name, next, prev, up, filename, friendly=None):
        node = (name, next, prev, up, filename, friendly or name)
        # add this file to dict
        # retrieve list with self.filenames.values()
        self.filenames[filename] = filename
        # add this node to nodelist
        self.nodeindex[name] = len(self.nodelist)
        self.nodelist.append(node)
        # set 'current' for menu items
        self.current = name
        self.menudict[self.current] = []
    
    def set_friendly(self, name, friendly):
        node = self.nodelist[self.nodeindex[name]]
        self.nodelist[self.nodeindex[name]] = node[:5] + (friendly,)

    def menuitem(self, nodename):
        menu = self.menudict[self.current]
        menu.append(nodename)

    def settitle(self, title):
        self.title = title


    def addimage(self, imagename):
        self.filenames[imagename] = imagename

    def index(self, args, nodename):
        self.indexlist.add((args, nodename))

    def beginmenu(self):
        pass

    def endmenu(self):
        pass

    def finalize(self):
        if not self.helpbase:
            return

        # generate interesting filenames
        resultfile   = self.helpbase + '.chm'
        projectfile  = self.helpbase + '.hhp'
        contentfile  = self.helpbase + '.hhc'
        indexfile    = self.helpbase + '.hhk'

        # use a reasonable title
        title        = self.title or self.helpbase

        # get the default topic file
        (topname, topnext, topprev, topup, topfile, topfriendly) = self.nodelist[0]
        defaulttopic = topfile

        # PROJECT FILE
        try:
            with open(projectfile, 'w', encoding='ascii', errors='xmlcharrefreplace') as fp:
                print('[OPTIONS]', file=fp)
                print('Auto Index=Yes', file=fp)
                print('Binary TOC=No', file=fp)
                print('Binary Index=Yes', file=fp)
                print('Compatibility=1.1', file=fp)
                print('Compiled file=' + resultfile + '', file=fp)
                print('Contents file=' + contentfile + '', file=fp)
                print('Default topic=' + defaulttopic + '', file=fp)
                print('Error log file=ErrorLog.log', file=fp)
                print('Index file=' + indexfile + '', file=fp)
                print('Title=' + title + '', file=fp)
                print('Display compile progress=Yes', file=fp)
                print('Full-text search=Yes', file=fp)
                print('Default window=main', file=fp)
                print('', file=fp)
                print('[WINDOWS]', file=fp)
                print('main=,"' + contentfile + '","' + indexfile
                            + '","","",,,,,0x63520,220,0x10384e,[0,0,1024,768],,,,,,,0', file=fp)
                print('', file=fp)
                print('[FILES]', file=fp)
                print('', file=fp)
                self.dumpfiles(fp)
        except IOError as msg:
            print(projectfile, ':', msg)
            sys.exit(1)

        # CONTENT FILE
        try:
            with open(contentfile, 'w', encoding='ascii', errors='xmlcharrefreplace') as fp:
                print('<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML//EN">', file=fp)
                print('<!-- This file defines the table of contents -->', file=fp)
                print('<HTML>', file=fp)
                print('<HEAD>', file=fp)
                print('<meta name="GENERATOR" '
                            'content="Microsoft&reg; HTML Help Workshop 4.1">', file=fp)
                print('<!-- Sitemap 1.0 -->', file=fp)
                print('</HEAD>', file=fp)
                print('<BODY>', file=fp)
                print('   <OBJECT type="text/site properties">', file=fp)
                print('     <param name="Window Styles" value="0x800025">', file=fp)
                print('     <param name="comment" value="title:">', file=fp)
                print('     <param name="comment" value="base:">', file=fp)
                print('   </OBJECT>', file=fp)
                self.dumpnodes(fp)
                print('</BODY>', file=fp)
                print('</HTML>', file=fp)
        except IOError as msg:
            print(contentfile, ':', msg)
            sys.exit(1)

        # INDEX FILE
        try:
            with open(indexfile, 'w', encoding='ascii', errors='xmlcharrefreplace') as fp:
                print('<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML//EN">', file=fp)
                print('<!-- This file defines the index -->', file=fp)
                print('<HTML>', file=fp)
                print('<HEAD>', file=fp)
                print('<meta name="GENERATOR" '
                            'content="Microsoft&reg; HTML Help Workshop 4.1">', file=fp)
                print('<!-- Sitemap 1.0 -->', file=fp)
                print('</HEAD>', file=fp)
                print('<BODY>', file=fp)
                print('<OBJECT type="text/site properties">', file=fp)
                print('</OBJECT>', file=fp)
                self.dumpindex(fp)
                print('</BODY>', file=fp)
                print('</HTML>', file=fp)
        except IOError as msg:
            print(indexfile  , ':', msg)
            sys.exit(1)
        
        if self.do_build:
            hhc = "C:\Program Files (x86)\HTML Help Workshop\hhc.exe"
            if platform.machine() != "AMD64":
                hhc = hhc.replace(" (x86)", "")
            if os.name == "nt" and os.path.exists(hhc):
                chmcmd = [hhc, self.helpbase + ".hhp"]
            else:
                chmcmd = ["chmcmd", "--no-html-scan", self.helpbase + ".hhp"]
            subprocess.call(chmcmd)

    def dumpfiles(self, outfile=sys.stdout):
        filelist = sorted(self.filenames.values())
        for filename in filelist:
            print(filename, file=outfile)

    def dumpnodes(self, outfile=sys.stdout):
        self.dumped = {}
        if self.nodelist:
            nodename, dummy, dummy, dummy, dummy, dummy = self.nodelist[0]
            self.topnode = nodename

        print('<UL>', file=outfile)
        for node in self.nodelist:
            self.dumpnode(node, 0, outfile)
        print('</UL>', file=outfile)

    def dumpnode(self, node, indent=0, outfile=sys.stdout):
        if node:
            # Retrieve info for this node
            (nodename, next, prev, up, filename, friendly) = node
            self.current = nodename

            # Have we been dumped already?
            if nodename in self.dumped:
                return
            self.dumped[nodename] = 1

            # Print info for this node
            print(' '*indent, end=' ', file=outfile)
            print('<LI><OBJECT type="text/sitemap">', end=' ', file=outfile)
            print('<param name="Name" value="' + friendly +'">', end=' ', file=outfile)
            print('<param name="Local" value="'+ filename +'">', end=' ', file=outfile)
            print('</OBJECT>', file=outfile)

            # Does this node have menu items?
            try:
                menu = self.menudict[nodename]
                self.dumpmenu(menu,indent+2,outfile)
            except KeyError:
                pass

    def dumpmenu(self, menu, indent=0, outfile=sys.stdout):
        if menu:
            currentnode = self.current
            if currentnode != self.topnode:    # XXX this is a hack
                print(' '*indent + '<UL>', file=outfile)
                indent += 2
            for item in menu:
                menunode = self.getnode(item)
                self.dumpnode(menunode,indent,outfile)
            if currentnode != self.topnode:    # XXX this is a hack
                print(' '*indent + '</UL>', file=outfile)
                indent -= 2

    def getnode(self, nodename):
        try:
            index = self.nodeindex[nodename]
            return self.nodelist[index]
        except KeyError:
            return None
        except IndexError:
            return None

    # (args,nodename) == (key,location)
    def dumpindex(self, outfile=sys.stdout):
        locale.setlocale(locale.LC_COLLATE, locale.getlocale())
        print('<UL>', file=outfile)
        for (key,location) in sorted(self.indexlist, key=lambda i: locale.strxfrm(i[0])):
            key = self.codeexpand(key)
            location = makefile(location)
            location = self.dirname + '/' + location
            print('<LI><OBJECT type="text/sitemap">', end=' ', file=outfile)
            print('<param name="Name" value="' + key.replace('"', "&quot;") + '">', end=' ', file=outfile)
            print('<param name="Local" value="' + location + '">', end=' ', file=outfile)
            print('</OBJECT>', file=outfile)
        print('</UL>', file=outfile)

    def codeexpand(self, line):
        co = self.codeprog.match(line)
        if not co:
            return line
        bgn, end = co.span(0)
        a, b = co.span(1)
        line = line[:bgn] + line[a:b] + line[end:]
        return line


class EPUB(HTMLHelp):
    def finalize(self):
        if not self.helpbase:
            return
        uid = uuid.uuid4()
        resultfile   = self.helpbase + '.epub'
        projectfile  = self.helpbase + '.opf'
        contentfile  = self.helpbase + '.ncx'
        title        = self.title or self.helpbase
        self.play_order = 0
        self.play_orders = {}
        self.mapping = {}
        actually_written = sorted(self.filenames.values())
        try:
            with open(projectfile, 'w', encoding='ascii', errors='xmlcharrefreplace') as fp:
                print('<?xml version="1.0"?>', file=fp)
                print('<package version="2.0" xmlns="http://www.idpf.org/2007/opf" '
                      'unique-identifier="uid">', file=fp)
                print('<metadata xmlns:dc="http://purl.org/dc/elements/1.1/" '
                      'xmlns:opf="http://www.idpf.org/2007/opf">', file=fp)
                print(f'<dc:title>{title}</dc:title>', file=fp)
                print(f'<dc:language>en</dc:language>', file=fp)
                print(f'<dc:identifier id="uid">{uid!s}</dc:identifier>', file=fp)
                print(f'</metadata>', file=fp)
                print('<manifest>', file=fp)
                actually_written = self.dumpfiles(fp)
                print(f'<item id="{contentfile}" href="{contentfile}" '
                      'media-type="application/x-dtbncx+xml"/>', file=fp)
                print('</manifest>', file=fp)
                print(f'<spine toc="{contentfile}">', file=fp)
                for (nodename, nxt, prev, up, fn, friendly) in self.nodelist:
                    fid = filename_to_xmlid(os.path.basename(fn))
                    print(f'<itemref idref="{fid}" />', file=fp)
                print('</spine>', file=fp)
                print('</package>', file=fp)
                print('', file=fp)
        except IOError as msg:
            print(projectfile, ':', msg)
            sys.exit(1)

        # CONTENT AND INDEX FILE
        try:
            with open(contentfile, 'w', encoding='ascii', errors='xmlcharrefreplace') as fp:
                print('<?xml version="1.0"?>', file=fp)
                print('<!DOCTYPE ncx PUBLIC "-//NISO//DTD ncx 2005-1//EN" '
                      '"http://www.daisy.org/z3986/2005/ncx-2005-1.dtd">', file=fp)
                print('<ncx>', file=fp)
                print('<head>', file=fp)
                print(f'<meta name="dtb:uid" content="{uid!s}"/>', file=fp)
                depth = self.sound_depth()
                print(f'<meta name="dtb:depth" content="{depth:d}"/>', file=fp)
                print('<meta name="dtb:totalPageCount" content="0"/>', file=fp)
                print('<meta name="dtb:maxPageNumber" content="0"/>', file=fp)
                print('</head>', file=fp)
                print(f'<docTitle><text>{title}</text></docTitle>', file=fp)
                self.dumpnodes(fp)
                self.dumpindex(fp)
                print('</ncx>', file=fp)
        except IOError as msg:
            print(contentfile, ':', msg)
            sys.exit(1)
 
        with zipfile.ZipFile(resultfile, "w", compression=zipfile.ZIP_DEFLATED,
                             compresslevel=9) as outfile:
            outfile.writestr("mimetype", "application/epub+zip", compress_type=zipfile.ZIP_STORED)
            with io.TextIOWrapper(outfile.open("META-INF/container.xml", "w")) as metafile:
                print('<?xml version="1.0"?>', file=metafile)
                print('<container version="1.0" '
                      'xmlns="urn:oasis:names:tc:opendocument:xmlns:container">', file=metafile)
                print('<rootfiles>', file=metafile)
                print(f'<rootfile full-path="{projectfile}" '
                      'media-type="application/oebps-package+xml"/>', file=metafile)
                print('</rootfiles>', file=metafile)
                print('</container>', file=metafile)
            outfile.write(projectfile, projectfile)
            outfile.write(contentfile, contentfile)
            for fn in actually_written:
                outfile.write(fn, fn)

    def dumpfiles(self, outfile=sys.stdout):
        actually_written = []
        for fn in sorted(self.filenames.values()):
            fid = filename_to_xmlid(os.path.basename(fn))
            mime = "application/octet-stream"
            if fn.endswith(".png"):
                mime = "image/png"
            elif fn.endswith(".css"):
                mime = "text/css"
            elif fn.endswith(".html"):
                # Conversion process of HTML to XML
                mime = "application/xhtml+xml"
                fn2 = fn[:-5] + ".xhtml"
                print(f"Converting HTML {fn!r} to XML {fn2!r}")
                with open(fn, "rb") as fpi:
                    document = html5lib.parse(fpi.read(), "dom")
                # Change all links to point to the .xhtml converted ones.
                for element in document.getElementsByTagName("*"):
                    if not element.hasAttribute("href"):
                        continue
                    href = element.getAttribute("href")
                    if "//" in href:
                        continue
                    anchor = ""
                    if "#" in href:
                        href, anchor = href.split("#")
                        anchor = "#" + anchor
                    if not href.endswith(".html"):
                        continue
                    element.setAttribute("href", href[:-5] + ".xhtml" + anchor)
                # XHTML1 is very anal about where <a> tags appear, so fix anchors in wrong context.
                for element in document.getElementsByTagName("a"):
                    if element.hasAttribute("href"):
                        continue
                    elif element.parentNode.nodeName in ("body",):
                        new = document.createElement("div")
                        new.setAttribute("id", element.getAttribute("id"))
                        element.parentNode.insertBefore(new, element)
                        element.parentNode.removeChild(element)
                    elif element.parentNode.nodeName == "dl":
                        first_dt = element.parentNode.getElementsByTagName("dt")[0]
                        first_dt.insertBefore(element, first_dt.firstChild)
                # "&gt;" in CSS selectors isn't always handled correctly by all EPUB readers
                for element in document.getElementsByTagName("style"):
                    css = element.firstChild
                    element.removeChild(css)
                    element.appendChild(document.createTextNode("/*"))
                    element.appendChild(document.createCDATASection(f"*/{css.data}/*"))
                    element.appendChild(document.createTextNode("*/"))
                # For EPUB we need to use an XHTML1 doctype, not an HTML4 or HTML5 doctype.
                document.childNodes[0].publicId = "-//W3C//DTD XHTML 1.1//EN"
                document.childNodes[0].systemId = "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"
                document.documentElement.setAttribute("xmlns", "http://www.w3.org/1999/xhtml")
                with open(fn2, "w", encoding="ascii", errors='xmlcharrefreplace') as fpo:
                    fpo.write(document.toxml().split("?>", 1)[1])
                self.mapping[fn] = fn2
                fn = fn2
            else:
                raise ValueError(f"unknown asset type {fn!r}")
            actually_written.append(fn)
            print(f'<item id="{fid}" href="{fn}" media-type="{mime}"/>', file=outfile)
        return actually_written
    
    def sound_depth(self, nodename = None):
        if nodename is None:
            return max(self.sound_depth(i[0]) for i in self.nodelist)
        elif nodename not in self.menudict or not self.menudict[nodename]:
            return 1
        else:
            return 1 + max(self.sound_depth(i) for i in self.menudict[nodename])

    def dumpnodes(self, outfile=sys.stdout):
        self.dumped = {}
        if self.nodelist:
            nodename, dummy, dummy, dummy, dummy, dummy = self.nodelist[0]
            self.topnode = nodename

        print('<navMap>', file=outfile)
        for node in self.nodelist:
            self.dumpnode(node, 0, outfile)
        print('</navMap>', file=outfile)

    def dumpnode(self, node, indent=0, outfile=sys.stdout):
        if node:
            # Retrieve info for this node
            (nodename, next, prev, up, filename, friendly) = node
            self.current = nodename
            fid = filename_to_xmlid(os.path.basename(filename)) + ".contents"

            # Have we been dumped already?
            if nodename in self.dumped:
                return
            self.dumped[nodename] = 1

            # Print info for this node
            self.play_order += 1
            self.play_orders[fid] = self.play_order
            print(' '*indent, end=' ', file=outfile)
            print(f'<navPoint id="{fid}" playOrder="{self.play_order}">', end=' ', file=outfile)
            print(f'<navLabel><text>{friendly}</text></navLabel>', end=' ', file=outfile)
            print('<content src="'+ self.mapping.get(filename, filename) +'"/>',
                    end=' ', file=outfile)

            # Does this node have menu items?
            try:
                menu = self.menudict[nodename]
            except KeyError:
                pass
            else:
                self.dumpmenu(menu,indent+2,outfile)
            
            # Close the node because XML
            print("</navPoint>", file=outfile)

    def dumpmenu(self, menu, indent=0, outfile=sys.stdout):
        if menu:
            currentnode = self.current
            if currentnode != self.topnode:    # XXX this is a hack
                indent += 2
            for item in menu:
                menunode = self.getnode(item)
                self.dumpnode(menunode,indent,outfile)
            if currentnode != self.topnode:    # XXX this is a hack
                indent -= 2

    # (args,nodename) == (key,location)
    def dumpindex(self, outfile=sys.stdout):
        print('<navList><navLabel><text>Index</text></navLabel>', file=outfile)
        locale.setlocale(locale.LC_COLLATE, locale.getlocale())
        for (key, location) in sorted(self.indexlist, key=lambda i: locale.strxfrm(i[0])):
            key = self.codeexpand(key)
            location = makefile(location)
            location = self.dirname + '/' + location
            fid = filename_to_xmlid(os.path.basename(location))
            self.play_order += 1
            if fid + '.contents' in self.play_orders:
                play_order = self.play_orders[fid  + '.contents']
                fid += f'.index{self.play_order:03d}'
            else:
                print(f'Doesn\'t match a contents node: {fid}')
                play_order = self.play_order
            print(f'<navTarget id="{fid}" playOrder="{play_order}">', end=' ', file=outfile)
            print('<navLabel><text>' + key.replace('&', '&amp;'
                                         ).replace('<', '&lt;'
                                         ).replace('>', '&gt;'
                                         ) + '</text></navLabel>', end=' ', file=outfile)
            print('<content src="' + self.mapping.get(location, location) + '"/>', 
                    end=' ', file=outfile)
            print('</navTarget>', file=outfile)
        print('</navList>', file=outfile)


# Put @var{} around alphabetic substrings
def makevar(string):
    if "@" in string:
        return "@var{" + string + "}"
    # NOTE: this depends on Python 3.7+ semantics of zero-length regex matches
    string2 = re.compile(r"\b(?!\w)").sub("}", string)
    string3 = re.compile(r"\b(?=\w)").sub("@var{", string2)
    return string3


# Split a string in "words" according to findwordend
def splitwords(str, minlength):
    words = []
    i = 0
    n = len(str)
    while i < n:
        while i < n and str[i] in ' \t\n': i = i+1
        if i >= n: break
        start = i
        i = findwordend(str, i, n)
        words.append(str[start:i])
    while len(words) < minlength: words.append('')
    return words


# Find the end of a "word", matching braces and interpreting @@ @{ @}
fwprog = re.compile('[@{} ]')
def findwordend(str, i, n):
    level = 0
    while i < n:
        mo = fwprog.search(str, i)
        if not mo:
            break
        i = mo.start()
        c = str[i]; i = i+1
        if c == '@': i = i+1 # Next character is not special
        elif c == '{': level = level+1
        elif c == '}': level = level-1
        elif c == ' ' and level <= 0: return i-1
    return n


# Convert a node name into a file name
def makefile(nodename):
    nodename = nodename.strip()
    return fixfunnychars(nodename) + '.html'


# Characters that are perfectly safe in filenames and hyperlinks
goodchars = string.ascii_letters + string.digits + '!@-=+.'

xmlidchars = string.ascii_letters + string.digits + '-._'

# Replace characters that aren't perfectly safe by dashes
# Underscores are bad since Cern HTTPD treats them as delimiters for
# encoding times, so you get mismatches if you compress your files:
# a.html.gz will map to a_b.html.gz

capitals = {}
def fixfunnychars(addr):
    # Sweep 1: Capital encoding (prevent clobbering on non-case-sensitive systems)
    if addr.casefold() not in capitals:
        capitals[addr.casefold()] = addr
    elif capitals[addr.casefold()] != addr:
        for i in addr[:]:
            if i.lower() != i:
                addr += "="
            else:
                addr += "@"
    # Sweep 2: Escaping
    i = 0
    while i < len(addr):
        c = addr[i]
        if c == "-": # Escape the escape char
            c = '--'
            addr = addr[:i] + c + addr[i+1:]
        elif c == " ": # Short escape for space
            c = '-@'
            addr = addr[:i] + c + addr[i+1:]
        elif c == "_": # Short escape for underscore
            c = '-='
            addr = addr[:i] + c + addr[i+1:]
        elif c not in goodchars: # Hex escapes
            c = '-' + binascii.hexlify(c.encode("utf-8")).decode("ascii")
            addr = addr[:i] + c + addr[i+1:]
        i = i + len(c)
    return addr

def filename_to_xmlid(filename):
    out = ["id_"]
    for c in filename:
        if c in xmlidchars:
            out.append(c)
        else:
            out.append("_" + binascii.hexlify(c.encode("utf-8")).decode("ascii"))
    return "".join(out)


# Increment a string used as an enumeration
def increment(s):
    if not s:
        return '1'
    for sequence in string.digits, string.ascii_lowercase, string.ascii_uppercase:
        lastc = s[-1]
        if lastc in sequence:
            i = sequence.index(lastc) + 1
            if i >= len(sequence):
                if len(s) == 1:
                    s = sequence[0]*2
                    if s == '00':
                        s = '10'
                else:
                    s = increment(s[:-1]) + sequence[0]
            else:
                s = s[:-1] + sequence[i]
            return s
    return s # Don't increment

refcounts_regex = re.compile("^Return value: (New reference|Borrowed reference|Always NULL).$")

# Enable dealing with in-line anchor links to later anchors by creating dummies in an inital sweep
class _DummyNode:
    def __init__(self, name):
        self.name = name
def rip_anchors(fp):
    node = 'Top'
    anchors = {}
    for line in fp:
        if line.startswith("@node "):
            node = line[6:].rstrip().split(",", 1)[0]
        else:
            for anchor in line.split("@anchor{")[1:]:
                anchor = anchor.split("}", 1)[0]
                anchors[anchor] = _DummyNode(node)
    return anchors

_codespan_regex = re.compile(r"(?<!@)@(?:code|asis)\{(.*?)(?<!@)\}")
def remove_codespans(title):
    return _codespan_regex.sub(r"\1", title).replace("@comma{}", ",")

def construct_delimited_names(fp):
    stack = []
    numbers = []
    friendlies = {}
    contents = []
    choose = lambda node, title: remove_codespans(title) \
                                 if "@" not in remove_codespans(title) \
                                 else node
    implicit_ups = {}
    escaped = False
    menu = False
    index_node = None
    has_modindex = False
    for line in fp:
        if escaped:
            if line.startswith("@end verbatim"):
                escaped = False
            continue
        elif menu:
            if line.startswith("@end menu"):
                menu = False
            elif line.startswith("* "):
                if "::" in line:
                    implicit_ups[line[1:].lstrip().split("::", 1)[0]] = stack[-1]
                else:
                    implicit_ups[line.split(":", 1)[1].split(".", 1)[0].strip()] = stack[-1]
            continue
        elif line.startswith("@node "):
            node, nxt, last, up = (line[6:].strip().split(",") + ["","",""])[:4]
            up = up or implicit_ups.get(node, "")
            numbers.append(0)
            while stack and stack[-1] != up:
                numbers.pop()
                stack.pop()
            stack.append(node)
            if node == "Python Module Index":
                has_modindex = True
        elif line.startswith("@top "):
            numbers.pop()
            friendlies[node] = line.strip().split(None, 1)[1]
        elif line.startswith(("@chapter ", "@section ", "@subsection ", "@subsubsection "),):
            if numbers:
                numbers[-1] += 1
            number_prefix = ".".join(str(i) for i in numbers) + ". " if numbers else ""
            friendlies[node] = number_prefix + choose(node, line.strip().split(None, 1)[-1])
            contents.append((len(stack) - 2, friendlies[node], node))
        elif line.startswith(("@unnumbered ", "@appendix ",
                              "@unnumberedsec ", "@appendixsec ",
                              "@unnumberedsubsec ", "@appendixsubsec ",
                              "@unnumberedsubsubsec ", "@appendixsubsubsec ")):
            friendlies[node] = choose(node, line.strip().split(None, 1)[-1])
            contents.append((len(stack) - 2, friendlies[node], node))
        elif line.startswith("@verbatim"):
            escaped = True
        elif line.startswith("@menu"):
            menu = True
        elif line.startswith("@printindex"):
            index_node = node
    return friendlies, contents, index_node, has_modindex

def procure_encoding(fn):
    with open(fn, "rb") as f:
        b = f.read(512)
    #
    # (1) Look at the "\input" and any BOM at the start and flag up any blatantly obvious encoding.
    # If the @documentencoding is present and contradicts this, it is obviously incorrect.
    if b.startswith(b'\xef\xbb\xbf'):
        return "utf-8-sig" # "utf-8" won't strip BOM
    elif b.startswith(b'\xff\xfe\\\x00'): # since \xff\xfe also starts the UTF-32 BOM
        return "utf-16" # "utf-16le" won't strip BOM
    elif b.startswith(b'\xff\xfe\x00\x00'):
        return "utf-32" # "utf-32le" won't strip BOM
    elif b.startswith(b'\xfe\xff'):
        return "utf-16" # "utf-16be" won't strip BOM
    elif b.startswith(b'\x00\x00\xfe\xff'):
        return "utf-32" # "utf-32be" won't strip BOM
    elif b.startswith(b'\\\x00i\x00'): # See part (2) for utf-16be
        return "utf-16le"
    elif b.startswith(b'\\\x00\x00\x00i\x00\x00\x00'):
        return "utf-32le"
    elif b.startswith(b'\x00\x00\x00\\\x00\x00\x00i'):
        return "utf-32be"
    #
    # (2) Try to pick out a suitable trial encoding looking at the "\input" at the start.
    # This is the decoder which will be used to look for the @documentencoding
    if b.startswith(b'\x00\\\x00i'):
        # Could in theory be cseucfixwid (not supported by stdlib out the bat but there's
        # nothing stopping sitecustomize from adding a codec) which matches UTF-16 for most ASCII.
        trial_encoding = "utf-16be"
    elif b[1:].startswith(b'\x89\x95\x97\xA4\xA3'):
        trial_encoding = "cp037" # EBCDIC
    else:
        trial_encoding = "latin1" # Default to ISO-8859-1 (note "latin1" decoder never errors out)
    #
    # (3) Look for a @documentencoding in the text.
    # Error handler: e.g. the 512 bytes might have a truncated UTF-16 4-byte code at the end.
    text = b.decode(trial_encoding, errors="replace")
    if "@documentencoding " in text:
        return text.split("@documentencoding ", 1)[1].split(None, 1)[0]
    elif "@c -*-" in text: # Emacs modeline
        modeline = text.split("@c -*-", 1)[1].split("-*-", 1)[0].strip()
        if "coding:" in modeline:
            return modeline.split("coding:", 1)[1].split(";", 1)[0].strip()
    #
    # (4) Default to the trial encoding
    return trial_encoding


def main():
    debugging = 0
    print_headers = 0
    cont = 0
    html5 = 0
    helpbase = ''
    helpclass = HTMLHelp
    do_build = False

    options, argv = getopt.gnu_getopt(sys.argv[1:], "dpc5H:E:b")
    for arg, val in options:
        if arg == '-d':
            debugging += 1
        elif arg == '-p':
            print_headers = 1
        elif arg == '-c':
            cont = 1
        elif arg == '-5':
            html5 = 1
        elif arg == '-H':
            helpclass = HTMLHelp
            helpbase = val
        elif arg == '-E':
            helpclass = EPUB
            helpbase = val
        elif arg == '-b':
            do_build = True
    if len(argv) != 2:
        print('usage: texi2hh [-d [-d]] [-p] [-c] [-5] [[-b] -H htmlhelp || -E epub]', \
              'inputfile outputdirectory')
        sys.exit(2)

    if html5:
        parser = TexinfoParserHTML5()
    else:
        parser = TexinfoParser()
    parser.cont = cont
    parser.debugging = debugging
    parser.print_headers = print_headers

    file = argv[0]
    dirname = argv[1]
    parser.setdirname(dirname)
    parser.setincludedir(os.path.dirname(file))

    if helpbase:
        htmlhelp = helpclass(helpbase, dirname, do_build)
        parser.sethtmlhelp(htmlhelp)
        parser.charset = "ascii"

    try:
        encoding = procure_encoding(file)
        print("### Input encoding:", encoding)
        fp = open(file, 'r', encoding=encoding)
    except IOError as msg:
        print(file, ':', msg)
        sys.exit(1)

    with fp:
        parser.friendlies, parser.pregen_contents, parser.index_node, parser.has_modindex = construct_delimited_names(fp)
        fp.seek(0)
        parser.anchors = rip_anchors(fp)
        fp.seek(0)
        parser.parse(fp)
    parser.report()

    if helpbase: 
        htmlhelp.finalize()


if __name__ == "__main__":
    main()
