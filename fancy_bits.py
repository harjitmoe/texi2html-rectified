#! /usr/bin/env python3
# -*- mode: python; coding: utf-8 -*-

import sys, re
import texi2html_rectified

# The HTML/CSS snippets were taken from the Python 2.5 HTML documentation and processed

makefile = texi2html_rectified.makefile

class FancyMixin:
    gui_assets = ("blank.png", "contents.png", "index.png", "lib.css", "modules.png",
                  "next.png", "previous.png", "up.png")
    
    def get_release_info(self):
        return '<div class="release-info">' + self.release_info + '</div>'

    def output_links(self):
        fancynav=self.fancynav_prefix
        #
        # "Previous" button (not necessarily previous at par)
        if self.rcont not in self.nulls:
            fancynav += self.fancynav_item % (makefile(self.rcont), "Previous", "Prev", "previous.png")
        elif self.prev not in self.nulls:
            fancynav += self.fancynav_item % (makefile(self.prev), "Previous", "Prev", "previous.png")
        elif self.up not in self.nulls:
            fancynav += self.fancynav_item % (makefile(self.up), "Previous", "Prev", "previous.png")
        else:
            fancynav += self.fancynav_dummy
        #
        # "Up" button
        if self.up not in self.nulls:
            fancynav += self.fancynav_item % (makefile(self.up), "Up", "Up", "up.png")
        elif self.topname != self.name:
            fancynav += self.fancynav_item % (makefile(self.topname), "Up", "Up", "up.png")
        else:
            fancynav += self.fancynav_dummy
        #
        # "Next" button (Py2.5's "Next" actually corresponds to our "cont")
        if (self.cont not in self.nulls) and (self.topname != self.name):
            fancynav += self.fancynav_item % (makefile(self.cont), "Next", "Next", "next.png")
        elif (self.next not in self.nulls) and (self.topname != self.name):
            fancynav += self.fancynav_item % (makefile(self.next), "Next", "Next", "next.png")
        else:
            fancynav += self.fancynav_dummy
        #
        # Node title heading (appearance)
        if not self.breaker:
            fancynav += self.fancynav_title % self.clean_title()
        else:
            fancynav += self.breaker
        #
        # "Table of Contents" button
        fancynav += self.fancynav_item % (makefile("Table of Contents"), "Table of Contents", "Table of Contents", "contents.png")
        #
        # "Module Index" button
        if self.has_modindex:
            fancynav += self.fancynav_item % (makefile("Python Module Index"), "Module Index", "Module Index", "modules.png")
        else:
            fancynav += self.fancynav_dummy
        #
        # "Index" button
        if self.index_node not in self.nulls:
            fancynav += self.fancynav_item % (makefile(self.index_node), "Index", "Index", "index.png")
        else:
            fancynav += self.fancynav_dummy
        #
        # Node title heading (sometimes)
        if self.breaker:
            fancynav += self.fancynav_title % self.clean_title()
        fancynav += self.fancynav_suffix
        self.write(fancynav)
        #
        # Textual bit. Note changes from standard behaviour.
        if self.topname == self.name:
            return # Don't show on title page
        # "Previous" link to previous node of any level if it exists, otherwise same as up.
        if (self.rcont not in self.nulls) and (self.rcont not in (self.up, self.next)):
            self.link('Previous', self.rcont, rel='prev', class_='sectref', label_element='b',
                      label_class='navlabel')
            self.write('\u2002')
        elif (self.prev not in self.nulls) and (self.prev != self.up):
            self.link('Previous', self.prev, rel='prev', class_='sectref', label_element='b',
                      label_class='navlabel')
            self.write('\u2002')
        elif self.up not in self.nulls:
            self.link('Previous', self.up, rel='prev', class_='sectref', label_element='b',
                      label_class='navlabel')
            self.write('\u2002')
        else:
            self.link('Previous', self.topname, rel='prev', class_='sectref', label_element='b',
                      label_class='navlabel')
            self.write('\u2002')
        # "Up" link to parent if it's explicitly assigned, else to the top node.
        if self.up not in self.nulls:
            self.link('Up', self.up, class_='sectref', label_element='b',
                      label_class='navlabel')
            self.write('\u2002')
        else:
            self.link('Up', self.topname, class_='sectref', label_element='b',
                      label_class='navlabel')
            self.write('\u2002')
        # "Next" link to what texi2html itself calls "Cont", i.e. the next node in flat sequence
        if (self.cont not in self.nulls) and (self.cont != self.next):
            self.link('Next', self.cont, rel='next', class_='sectref', label_element='b',
                      label_class='navlabel')
            self.write('\u2002')
        elif self.next not in self.nulls:
            self.link('Next', self.next, rel='next', class_='sectref', label_element='b',
                      label_class='navlabel')
            self.write('\u2002')
        # "Previous Over" / "Next Over" links (not included by Py2.5 but nonetheless useful)
        has_po = self.rcont and self.prev != self.rcont and self.prev not in self.nulls
        has_no = self.cont and self.next != self.cont and self.next not in self.nulls
        if has_po or has_no:
            self.write("<br /> (")
            if has_po:
                self.link('Previous Over', self.prev, class_='sectref', label_element='b',
                          label_class='navlabel')
            if has_po and has_no:
                self.write('\u2002')
            if has_no:
                self.link('Next Over', self.next, class_='sectref', label_element='b',
                          label_class='navlabel')
            self.write(")")

class FancyNodeTables(FancyMixin, texi2html_rectified.HTMLNode):
    breaker = None
    fancynav_prefix="""
          <table width="100%" cellpadding="0" cellspacing="2" role="presentation">
              <tr>
    """
    fancynav_dummy="""
                <td><img width="32" height="32" align="bottom" border="0" alt=""
                    src="blank.png"></td>
    """
    fancynav_title="""
                <td align="center" width="100%%">%s</td>
    """
    fancynav_item="""
                <td><a href="%s"
                    title="%s"><img width="32" height="32"
                      align="bottom" border="0" alt="%s"
                      src="%s"></a></td>
    """
    fancynav_suffix="""
              </tr>
          </table>
    """
    css="""
        <link rel="stylesheet" href="lib.css">
    """
    

class FancyNodeFloats(FancyMixin, texi2html_rectified.HTMLNode):
    breaker = """&ZeroWidthSpace;</span></div><div class=bannerright><span class=bannerspacing>&ZeroWidthSpace;"""
    fancynav_prefix = """<div class=navbanner><div class=bannerleft><span class=bannerspacing>"""
    fancynav_dummy = """ </span><img width="32" height="32" alt="" src="blank.png"><span class=bannerspacing> """
    fancynav_title = """</div><div class=bannermiddle>%s</div>"""
    fancynav_item = """ </span><a href="%s"
                    title="%s"><img width="32" height="32" alt="%s" src="%s"></a><span class=bannerspacing> """
    fancynav_suffix = """</div><div class=clear></div>"""
    css = """
        <link rel="stylesheet" href="lib.css" type="text/css">
        <style type="text/css">
            h1+br, h2+br, h3+br, h4+br, h5+br, h6+br{ display: none; }
            div.warning blockquote, div.note blockquote, div.warning blockquote>div, div.note blockquote>div { display: contents; }
            div.bannerleft { float: left; }
            div.bannermiddle { background-color:#99ccff;font-weight:bold;font-family:sans-serif;font-size:110%;text-align:center;line-height:32px;height:32px;overflow:hidden; }
            div.bannerright { float: right; }
            span.bannerspacing { font-size: xx-small; }
            div.navbanner {height:32px;margin:4px 0;}
            .clear{clear:both;}
        </style>
    """

class Fancy5NodeFlexes(FancyMixin, texi2html_rectified.HTML5Node):
    breaker = None
    fancynav_prefix = """
          <div class=navbanner>
    """
    fancynav_dummy = """
                <img width="32" height="32" alt="" src="blank.png">
    """
    fancynav_title = """
                <div>%s</div>
    """
    fancynav_item = """
                <a href="%s"
                    title="%s"><img width="32" height="32" alt="%s" src="%s"></a>
    """
    fancynav_suffix = """
          </div>
    """
    css = """
        <link rel="stylesheet" href="lib.css">
        <style type="text/css">
            h1+br, h2+br, h3+br, h4+br h5+br hs+br{ display: none; }
            div.warning blockquote, div.note blockquote, div.warning blockquote>div, div.note blockquote>div { display: contents; }
            .navbanner{display:flex;flex-wrap:nowrap;height:32px;margin:4px 0;}
            .navbanner>*{border-left:1px solid white;border-right:1px solid white;}
            .navbanner>a,.navbanner>img{flex-grow:0;}
            .navbanner>div{flex-grow:1;background-color:#99ccff;font-weight:bold;font-family:sans-serif;font-size:110%;text-align:center;line-height:32px;overflow:hidden;}
            figure{margin-left:unset;margin-right:unset;}
            figcaption{font-weight:bold;}
        </style>
    """


texi2html_rectified.TexinfoParser.Node = FancyNodeFloats
texi2html_rectified.TexinfoParserHTML5.Node = Fancy5NodeFlexes

if __name__ == "__main__":
    sys.argv.insert(1, "-c") # Enable cont mode
    texi2html_rectified.main()


